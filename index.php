<?php

require_once __DIR__ . "/api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/api/tools/array.php";
require_once __DIR__ . "/api/tools/awqot.php";
require_once __DIR__ . "/api/tools/configuration.php";
require_once __DIR__ . "/api/tools/database.php";
require_once __DIR__ . "/api/tools/is_online.php";
require_once __DIR__ . "/api/tools/time.php";
require_once __DIR__ . "/api/tools/timezone.php";

// ---------- MODEL ----------

$hijri_months = [
  "Muharram",
  "Safar",
  "Rabi' al-Awwal",
  "Rabi' al-Thani",
  "Jumada al-Awwal",
  "Jumada al-Thani",
  "Rajab",
  "Sha'aban",
  "Ramadhan",
  "Shawwal",
  "Dhu al-Qi'dah",
  "Dhu al-Hijjah",
];

$today_hijriah = explode("/", trim(shell_exec("idate -s")));
$today_hijriah["date"] = (int) $today_hijriah[0];
$today_hijriah["month"] = $hijri_months[((int) $today_hijriah[1]) - 1];
$today_hijriah["year"] = (int) $today_hijriah[2];

$hijri_date = $today_hijriah["date"] . " " . $today_hijriah["month"] . " " . $today_hijriah["year"];

$awqot_masjid_name = get_configuration("awqot_masjid_name");
$awqot_masjid_address = get_configuration("awqot_masjid_address");
$schedule_id = get_configuration("schedule_id");
$city_id = get_configuration("city_id");
$ihtiyat = get_configuration("ihtiyat");
$masjid_photo_path = get_configuration("awqot_masjid_photo") ?: 'THIS_FILE_DOES_NOT_EXIST';

$is_masjid_photo_exist = file_exists(__DIR__ . "/data/" . $masjid_photo_path);

$current_timezone = get_timezone();

$city = execute_sql("
  SELECT
    cities.province_id AS province_id,
    cities.name AS name
  FROM cities
  WHERE cities.id = :city_id
", [
  ":city_id" => [$city_id, PDO::PARAM_INT],
])->fetch();

$province = execute_sql("
  SELECT provinces.name AS name
  FROM provinces
  WHERE provinces.id = :province_id
", [
  ":province_id" => [$city["province_id"], PDO::PARAM_INT],
])->fetch();

$time_packs = execute_sql("
  SELECT id, name
  FROM time_packs
  WHERE is_disabled = 0
  ORDER BY id ASC
")->fetchAll();

$now = round(microtime(true) * 1000);
$scheduled_times = json_decode(file_get_contents(__DIR__ . "/data/scheduled_times.json"), true);

usort($scheduled_times, function($a, $b) {
  return $a['audio_begin_time'] <=> $b['audio_begin_time'];
});

$scheduled_times = array_map(function ($scheduled_time) use ($now, $time_packs) {
  $time_pack = array_find($time_packs, function ($time_pack) use ($scheduled_time) {
    return $scheduled_time["time_pack_id"] === $time_pack["id"];
  });
  $audio_end_time = $scheduled_time["audio_begin_time"] + $scheduled_time["total_duration"];
  $ihtiyat_second = (int) get_configuration("ihtiyat_{$time_pack["name"]}");
  return [
    "is_past" => $scheduled_time["audio_begin_time"] < $now,
    "is_present" => $scheduled_time["audio_begin_time"] < $now && $audio_end_time > $now,
    "is_future" => $scheduled_time["audio_begin_time"] > $now,
    "time_name" => $time_pack["name"],
    "begin_time" => humanize_time($scheduled_time["audio_begin_time"]),
    "end_time" => humanize_time($audio_end_time),
    "audio_begin_time" => $scheduled_time["audio_begin_time"],
    "audios" => $scheduled_time["audios"],
    "total_duration" => $scheduled_time["total_duration"],
    "ihtiyat_second" => $ihtiyat_second,
    "ihtiyat" => humanize_duration($ihtiyat_second * 1000),
    "timing_basis" => $scheduled_time["timing_basis"],
  ];
}, $scheduled_times);

$scheduled_time_next = array_find($scheduled_times, function ($scheduled_time) use ($now) {
  return $scheduled_time["audio_begin_time"] > $now;
}) ?: $scheduled_times[0];

$audios = array_map(function ($audio_hash) {
  return execute_sql("
    SELECT
      audios.filename AS filename, 
      audios.duration AS duration
    FROM audios
    WHERE audios.hash = :audio_hash
  ", [
    ":audio_hash" => [$audio_hash, PDO::PARAM_STR],
  ])->fetch();
}, $scheduled_time_next["audios"]);

$audios = array_filter($audios, function ($audio) {
  return $audio;
});

$scheduling_compute_time = get_scheduling_time();

require_once __DIR__ . "/components/awqot_admin.php";
require_once __DIR__ . "/components/awqot_time.php";
require_once __DIR__ . "/components/basic_style.php";
require_once __DIR__ . "/components/box.php";
require_once __DIR__ . "/components/button.php";
require_once __DIR__ . "/components/button_group.php";
require_once __DIR__ . "/components/check_ds3231.php";
require_once __DIR__ . "/components/check_scheduling.php";
require_once __DIR__ . "/components/head.php";
require_once __DIR__ . "/components/icon.php";
require_once __DIR__ . "/components/notification.php";
require_once __DIR__ . "/components/page.php";
require_once __DIR__ . "/components/register_notice.php";
require_once __DIR__ . "/components/sidebar.php";
require_once __DIR__ . "/components/table.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Beranda - Awqot</title>
  <?php publish("head"); ?>
</head>

<body>
  <?php publish("body"); ?>
  <main class="page" style="padding-top: 3.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <span class="icon">
              <?php include __DIR__ . "/static/icons/round-menu-24px.svg" ?>
            </span>
          </button>
          <h1 class="page-title">Beranda</h1>
        </div>
      </div>
    </header>
    <div class="page-content">
      <?php if ($is_masjid_photo_exist): ?>
      <div
        style="
          background-image: url('/data/<?= $masjid_photo_path ?>');
          background-position: center;
          background-size: cover;
          background-attachment: fixed;
          height: 300px;
          display: flex;
          justify-content: center;
          align-items: flex-end;
          margin-bottom: 1rem;
        "
      >
        <p
          style="
            box-sizing: border-box;
            width: 100%;
            margin: 0;
            padding: 1rem;
            background-color: rgba(255, 255, 255, .8);
            font-size: 1.5rem;
            text-align: center;
          "
        ><?= $awqot_masjid_name ?><br><span style="display: block; font-size: .9rem; font-weight: 400; line-height: 1.25;"><?= $awqot_masjid_address ?></span></p>
      </div>
      <?php else: ?>
      <p>
        <img
          src="/static/icons/awqot-brand-plain.png"
          alt="Logo"
          style="
            display: block;
            width: 12rem;
            margin: 2rem auto;
          "
        >
      </p>
      <?php endif; ?>
      <p
        style="
          margin: 0 auto .75rem auto;
          font-size: 1.3rem;
          font-weight: 300;
          text-align: center;
        "
      >Jadwal selanjutnya:</p>
      <p
        id="countdown-time"
        data-time="<?= $scheduled_time_next["audio_begin_time"] ?>"
        style="
          margin: 0 auto;
          font-size: 2rem;
          font-weight: 300;
          font-family: monospace;
          text-align: center;
        "
      >--:--:--</p>
      <p
        style="
          padding: 0 1.5rem;
          font-size: 1.25rem;
          font-weight: 300;
          line-height: 1.5;
          text-align: center;
        "
      >
        Waktu di Awqot <span id="awqot-time" data-time="<?= $now ?>" style="font-family: Fira Code; font-size: .9em; font-weight: 500;">--:--:--</span> <?= $current_timezone ?>
        <br><span style="font-weight: 400; font-size: 1.1rem;"><?= date('d F Y') ?></span> / <span style="font-weight: 400; font-size: 1.1rem;"><?= $hijri_date ?></span>
        <br>Jadwal wilayah <a href="/pages/timezone.php" style="text-transform: capitalize"><?= strtolower($province["name"]) ?> <?= strtolower($city["name"]) ?></a>
      </p>

      <div style="padding: 0 1rem; box-sizing: border-box;">
        <section class="box">
          <div class="box-content">
            <table class="table rounded">
              <thead>
                <tr>
                  <th colspan="2">Jadwal hari ini</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($scheduled_times as $scheduled_time): ?>
                <tr
                  style="
                    <?= $scheduled_time === $scheduled_time_next ? "font-weight: 500;" : "" ?>
                  "
                >
                  <td><?= $scheduled_time["time_name"] ?></td>
                  <td
                    style="
                      text-align: center;
                      font-variant-numeric: tabular-nums;
                    "
                  ><?= $scheduled_time["begin_time"] ?>-<?= $scheduled_time["end_time"] ?><?php if ($scheduled_time["timing_basis"] === "kemenag" && $scheduled_time["ihtiyat_second"] !== 0): ?> (<?= $scheduled_time["ihtiyat_second"] > 0 ? "+" : "" ?><?= $scheduled_time["ihtiyat"] ?>)<?php endif; ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </section>
        <section class="box">
          <div class="box-content">
            <table class="table">
              <thead>
                <tr>
                  <th>Materi selanjutnya</th>
                  <th>Durasi</th>
                </tr>
              </thead>
              <?php if (count($audios) === 0): ?>
              <tbody>
                <tr>
                  <td colspan="2">
                    <p class="notification">Belum ada materi.</p>
                  </td>
                </tr>
              </tbody>
              <?php else: ?>
              <tbody>
                <?php foreach ($audios as $audio): ?>
                <tr>
                  <td class="ellipsis"><?= $audio["filename"] ?></td>
                  <td
                    style="
                      font-variant-numeric: tabular-nums;
                    "
                  ><?= humanize_duration($audio["duration"]) ?></td>
                </tr>
                <?php endforeach ?>
              </tbody>
              <tfoot>
                <tr>
                  <td style="text-align: right;">total:</td>
                  <td><strong><?= humanize_duration($scheduled_time_next["total_duration"]) ?></strong></td>
                </tr>
              </tfoot>
              <?php endif; ?>
            </table>
          </div>
        </section>
      </div>

      <p style="font-style: italic; font-size: .9rem; color: #616161;text-align: center;">terhitung sejak <span id="scheduling_compute_time" data-time="<?= $scheduling_compute_time ?>"></span></p>
    </div>
  </main>

  <script>
  var now = deviceTime;
  var countdownTimeEl = document.getElementById("countdown-time");
  var countdownTimeText = countdownTimeEl.childNodes[0];
  var countdownTime = parseInt(countdownTimeEl.dataset.time, 10);
  var awqotTimeEl = document.getElementById("awqot-time");
  var awqotTimeText = awqotTimeEl.childNodes[0];
  var awqotTime = awqotTime;
  var awqotTimeDiff = awqotTimeDiff;

  function toTwoDigit(number) {
    return number < 10 ? '0' + number.toString() : number;
  }

  function humanizeTime(date) {
    return toTwoDigit(date.getHours()) + ':' + toTwoDigit(date.getMinutes()) + ':' + toTwoDigit(date.getSeconds());
  }

  function refreshTime() {
    var date = new Date();
    var now = date.getTime();
    date.setMilliseconds(0);
    date.setSeconds(0);
    date.setMinutes(0);
    date.setHours(0);
    var awqot = now + awqotTimeDiff;
    var countdown = date.getTime() + (countdownTime - awqot);
    requestAnimationFrame(function (timePass) {
      timePass = timePass / 1000;
      if (!isNaN(countdownTime)) countdownTimeText.nodeValue = humanizeTime(new Date(countdown + timePass));
      awqotTimeText.nodeValue = humanizeTime(new Date(awqot + timePass));
    });
  }

  refreshTime();
  setInterval(refreshTime, 1000);

  scheduling_compute_time.textContent = new Date(parseInt(scheduling_compute_time.dataset.time, 10) * 1000).toLocaleString();
  </script>
</body>

</html>
