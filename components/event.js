(function () {
  var publishers = window.fh47_event_publishers = {};

  function getSubscribers(name) {
    return publishers[name] || (publishers[name] = []);
  }

  function subscribe(name, subscriber) {
    getSubscribers(name).push(subscriber);
  }

  function publish(name, data) {
    getSubscribers(name).forEach(function publication(subscriber) {
      subscriber(data);
    });
  }

  window.ev = {
    subscribe: subscribe,
    publish: publish,
  }
})();