<?php

require_once __DIR__ . "/animation.php";
require_once __DIR__ . "/basic_style.php";
require_once __DIR__ . "/event.php";
require_once __DIR__ . "/module.php";

subscribe("head", function () {
  print_script(__DIR__ . "/page_transition.js");
});

subscribe("body", function () {
  include_once __DIR__ . "/page_transition.html";
});
