ev.subscribe('modal:open', function modalFadeIn(data) {
  ev.publish('modal:fadein', data);
});

ev.subscribe('modal:close', function modalFadeOut(data) {
  ev.publish('modal:fadeout', data);
});

ev.subscribe('modal:fadein', function modalFadeIn(data) {
  var dialog = data.dialog;
  ev.publish('animate', {
    duration: 300,
    transition: function fadeIn(progress) {
      if (progress === 0) {
        dialog.setAttribute('open', true);
        ev.publish('modal:overflowcheck');
      }
      dialog.style.opacity = progress;
    },
  });
});

ev.subscribe('modal:fadeout', function modalFadeOut(data) {
  var dialog = data.dialog;
  ev.publish('animate', {
    duration: 300,
    transition: function fadeIn(progress) {
      dialog.style.opacity = 1 - progress;
      if (progress === 1) dialog.removeAttribute('open');
    },
  });
});

ev.subscribe('modal:resize', function modalOverflowCheck() {
  var modals = document.getElementsByClassName('modal');
  for (var index = 0; index < modals.length; index++) {
    var modal = modals.item(index);
    modal.style.width = window.innerWidth + 'px';
    modal.style.height = window.innerHeight + 'px';
  }
});

ev.subscribe('modal:overflowcheck', function modalOverflowCheck() {
  var modals = document.getElementsByClassName('modal-content');
  for (var index = 0; index < modals.length; index++) {
    var modalContent = modals.item(index);
  }
});

window.addEventListener('resize', function () {
  ev.publish('modal:resize');
  ev.publish('modal:overflowcheck');
});

window.addEventListener('orientationchange', function () {
  ev.publish('modal:resize');
  ev.publish('modal:overflowcheck');
});
