<?php

require_once __DIR__ . "/event.php";

function print_script($absolute_path) {
  echo "<script>";
  readfile($absolute_path);
  echo "</script>";
}

function print_style($absolute_path) {
  echo "<style>";
  readfile($absolute_path);
  echo "</style>";
}

subscribe("head", function () {
  print_script(__DIR__ . "/module.js");
});
