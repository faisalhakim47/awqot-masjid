<?php

require_once __DIR__ . "/../api/tools/database.php";

require_once __DIR__ . "/animation.php";
require_once __DIR__ . "/basic_style.php";
require_once __DIR__ . "/http.php";
require_once __DIR__ . "/event.php";
require_once __DIR__ . "/module.php";

subscribe("head", function () {
  print_script(__DIR__ . "/materi_selector.js");
});

subscribe("body", function () {
  ?>
<dialog id="materi_selector" class="modal">
  <div class="modal-overlay"></div>
  <form class="modal-shell" method="post" style="height: calc(100% - 2rem);">
    <header class="modal-header" style="border-bottom: 1px solid #BBBBBB;">
      <div class="modal-header-row">
        <div class="modal-header-left">
          <h4 class="modal-title">Pilih Materi</h4>
        </div>
        <div class="modal-header-right">
          <button
            type="button"
            class="button secondary"
            onclick="ev.publish('materi_selector:close');"
          >
            <span class="icon">
              <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
            </span>
          </button>
        </div>
      </div>
      <div class="modal-header-row" style="align-items: flex-start;">
        <div class="field">
          <div class="field-input">
            <span class="field-input-addons">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-search-24px.svg" ?>
              </span>
            </span>
            <input
              id="materi_selector-search-input"
              type="text"
              placeholder="Pencarian"
              oninput="ev.publish('materi_selector:relayout')"
            >
          </div>
        </div>
      </div>
    </header>
    <div class="modal-content" style="height: 100%;">
      <input type="hidden" name="action" value="materi_selector_input">
      <p
        id="materi_selector-search-notfound"
        class="notification"
        style="display: none;"
      >Tidak ditemukan materi dengan kata kunci.</p>
      <style>
      #materi_selector-search-content li {
        display: flex;
        height: 3rem;
        overflow: hidden;
        /*
        transition-duration: 300ms;
        transition-timing-function: ease-in-out;
        transition-property: height, opacity;
         */
      }
      #materi_selector-search-content .list-view-item-row {
        width: 100%;
        max-width: 100%;
        display: flex;
        justify-content: space-between;
      }
      #materi_selector-search-content .duration {
        padding-right: .75rem;
        color: #424242;
        font-size: .9rem;
      }
      </style>
      <p id="materi_selector-loading-placeholder" style="font-style: italic;">Memuat...</p>
      <ul id="materi_selector-search-content" class="list-view selectable" style="border-top: none;"></ul>
    </div>
    <footer class="modal-footer" style="border-top: 1px solid #BBBBBB;">
      <div class="modal-footer-left">
      </div>
      <div class="modal-footer-right">
        <button
          id="audio_upload_button"
          type="button"
          class="button secondary"
          onclick="document.getElementById('materi_selector_upload_input').click();"
        >
          <span class="icon">
            <?php include __DIR__ . "/../static/icons/round-cloud_upload-24px.svg" ?>
          </span>
          <span id="audio_upload_button_text" class="text">Upload</span>
        </button>
        <button type="submit" class="button primary">
          <span class="icon">
            <?php include __DIR__ . "/../static/icons/round-check-24px.svg" ?>
          </span>
          <span class="text">Tambahkan</span>
        </button>
      </div>
    </footer>
  </form>
</dialog>

<form
  id="materi_selector_upload_form"
  action="/api/audio_upload.php"
  method="post"
  enctype="multipart/form-data"
  style="display: none;"
>
  <input
    id="materi_selector_upload_input"
    type="file"
    name="audio_files[]"
    accept="audio/*"
    multiple="multiple"
    onchange="ev.publish('materi_selector:audio_upload')"
  >
</form>
  <?php
});
