<?php

require_once __DIR__ . "/../api/tools/is_online.php";

require_once __DIR__ . "/awqot_admin.php";
require_once __DIR__ . "/event.php";
require_once __DIR__ . "/http.php";
require_once __DIR__ . "/modal.php";
require_once __DIR__ . "/module.php";

if (is_online()) {
  subscribe("body", function () {
    include_once __DIR__ . "/register_notice.html";
  });

  subscribe("body", function () {
    print_script(__DIR__ . "/register_notice.js");
  });
}
