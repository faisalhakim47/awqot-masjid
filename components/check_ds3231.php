<?php

require_once __DIR__ . "/event.php";
require_once __DIR__ . "/http.php";
require_once __DIR__ . "/module.php";

subscribe("head", function () {
  print_script(__DIR__ . "/check_ds3231.js");
});

subscribe("body", function () {
  include_once __DIR__ . "/check_ds3231.html";
});
