<?php

require_once __DIR__ . "/basic_style.php";
require_once __DIR__ . "/event.php";
require_once __DIR__ . "/modal.php";
require_once __DIR__ . "/module.php";

subscribe("head", function () {
  print_script(__DIR__ . "/materi_player.js");
});

subscribe("body", function () {
  include_once __DIR__ . "/materi_player.html";
});
