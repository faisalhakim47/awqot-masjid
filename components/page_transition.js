window.addEventListener('load', function () {
  ev.publish('animate', {
    duration: 300,
    transition: function sreenFadeIn(progress) {
      if (progress === 0) splash_screen.style.display = 'block';
      splash_screen.style.opacity = 1 - progress;
      if (progress === 1) splash_screen.style.display = 'none';
    },
  });
});

window.addEventListener('beforeunload', function () {
  ev.publish('animate', {
    duration: 300,
    transition: function screenFadeOut(progress) {
      if (progress === 0) splash_screen.style.display = 'block';
      splash_screen.style.opacity = progress;
    },
  });
});
