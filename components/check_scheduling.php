<?php

require_once __DIR__ . "/../api/tools/awqot.php";

require_once __DIR__ . "/event.php";
require_once __DIR__ . "/module.php";

if (!is_scheduling_active()) {
  subscribe("head", function () {
    print_script(__DIR__ . "/check_scheduling.js");
  });
  
  subscribe("body", function () {
    include_once __DIR__ . "/check_scheduling.html";
  });
}
