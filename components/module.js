window._module = window._module || (function () {
    var moduleScripts = [];

    function registerScript(name, loader) {
        moduleScripts.push({
            name: name,
            requestPromise: null,
            request: loader,
        });
    }

    function loadStyle(url) {
        return new Promise(function (resolve, reject) {
            var link = document.createElement('link');
             link.rel = 'stylesheet';
             link.onload = resolve;
             link.onerror = reject;
             link.href = url;
             document.head.appendChild(link);
        });
    }

    function loadScript(url) {
        return new Promise(function (resolve, reject) {
            var script = document.createElement('script');
            script.addEventListener('load', resolve);
            script.addEventListener('error', reject);
            script.async = true;
            script.defer = true;
            script.src = url;
            document.head.appendChild(script);
        });
    }

    var isLoaded = false;
    var waitToLoadFns = [];
    function requireScript(scriptName) {
        var moduleScript = moduleScripts.find(function (moduleScript) {
            return moduleScript.name === scriptName;
        });
        if (moduleScript.requestPromise) {
            return moduleScript.requestPromise;
        }
        if (isLoaded) {
            moduleScript.requestPromise = moduleScript.request();
        } else {
            moduleScript.requestPromise = new Promise(function (resolve, reject) {
                waitToLoadFns.push(function () {
                    moduleScript.request()
                        .then(resolve)
                        .catch(reject);
                });
            });
        }
        return moduleScript.requestPromise;
    }

    window.addEventListener('load', function () {
        isLoaded = true;
        waitToLoadFns.forEach(function (fn) {
            fn();
        });
    });

    registerScript('jets', function () {
        return loadScript(location.origin + '/static/scripts/jets.0.14.1.min.js');
    });

    return {
        registerScript: registerScript,
        loadStyle: loadStyle,
        loadScript: loadScript,
        requireScript: requireScript,
    };
})();
