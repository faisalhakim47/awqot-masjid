<?php

require_once __DIR__ . "/event.php";
require_once __DIR__ . "/typography.php";
require_once __DIR__ . "/module.php";

subscribe("head", function () {
  print_style(__DIR__ . "/basic_style.css");
});
