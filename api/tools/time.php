<?php

function duration_to_time($total_milisaconds) {
  $total_second = $total_milisaconds / 1000;
  $total_minute = $total_second / 60;
  $total_hour = $total_minute / 60;
  $hour = floor($total_hour);
  $minute = floor($total_minute) - ($hour * 60);
  $second = floor($total_second) - ($minute * 60) - ($hour * 60 * 60);
  return [
    "hour" => $hour,
    "minute" => $minute,
    "second" => $second,
  ];
}

function humanize_duration($duration) {
  $is_negative = $duration < 0;
  if ($is_negative) $duration *= -1;
  $time = duration_to_time($duration);
  $formated = "";
  if ($time["hour"]) $formated .= "{$time["hour"]}j";
  if ($time["minute"]) $formated .= "{$time["minute"]}m";
  if ($time["second"]) $formated .= "{$time["second"]}d";
  if ($is_negative) $formated = "-" . $formated;
  return $formated;
}

function humanize_time($time) {
  $time = round($time / 1000);
  return date("H", $time) . ":" . date("i", $time);
}
