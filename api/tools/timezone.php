<?php

function get_timezone_options() {
  return [
    [ "label" => "WIB", "value" => "Asia/Jakarta" ],
    [ "label" => "WITA", "value" => "Asia/Makassar" ],
    [ "label" => "WIT", "value" => "Asia/Jayapura" ],
  ];
}

function get_timezones() {
  return array_map(function ($timezone_option) {
    return $timezone_option["label"];
  },get_timezone_options());
}

function get_timezone() {
  $current_timezone = trim(shell_exec("cat /etc/timezone"));
  foreach (get_timezone_options() as $timezone_option) {
    if ($timezone_option["value"] === $current_timezone) {
      return $timezone_option["label"];
    }
  }
}

function set_timezone($timezone) {
  foreach (get_timezone_options() as $timezone_option) {
    if ($timezone_option["label"] === $timezone) {
      shell_exec("sudo timedatectl set-timezone " . $timezone_option["value"]);
    }
  }
}
