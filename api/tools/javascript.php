<?php 

function export_to_js($variable_name, $value) {
  ?><script
    type="application/json"
    id="data_<?= $variable_name ?>"
    style="display: none;"
  ><?= json_encode($value) ?></script>
  <script>
    (function () {
      let el = document.getElementById('data_<?= $variable_name ?>');
      window.<?= $variable_name ?> = JSON.parse(el.innerText);
      el.parentNode.removeChild(el);
    })();
  </script>
  <?php
}
