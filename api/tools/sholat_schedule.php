<?php

require_once __DIR__ . "/database.php";

function ensure_sholat_schedule(int $city_id) {
  $new_times = array_map(function (string $row) {
    $columns = explode(" ", $row);
    $time_pack_id = (int) $columns[0];
    $month = (int) $columns[1];
    $date = (int) $columns[2];
    $hour = (int) $columns[3];
    $minute = (int) $columns[4];
    return [
      'key' => $month . '_' . $date . '_' . $time_pack_id,
      'time_pack_id' => $time_pack_id,
      'month' => $month,
      'date' => $date,
      'hour' => $hour,
      'minute' => $minute,
    ];
  }, explode("\n", file_get_contents(__DIR__ . "/../../data/schedules/" . $city_id . ".txt")));
  $old_times = execute_sql("
      SELECT
        times.hour AS hour,
        times.minute AS minute,
        CONCAT(times.month, '_', times.date, '_', times.time_pack_id) AS id
      FROM times
      WHERE city_id = :city_id
    ", [
      "city_id" => [$city_id, PDO::PARAM_INT],
    ])->fetchAll();
  $old_times = array_reduce($old_times, function ($map, $time) {
    $map[$time['id']] = [
      'hour' => (int) $time['hour'],
      'minute' => (int) $time['minute'],
    ];
    return $map;
  }, []);
  foreach ($new_times as $new_time) {
    if (isset($old_times[$new_time['key']])) {
      $old_time = $old_times[$new_time['key']];
      if (($old_time["hour"] !== $new_time['hour']) || ($old_time["minute"] !== $new_time['minute'])) {
        execute_update_sql("times", [
          "hour" => [$new_time['hour'], PDO::PARAM_INT],
          "minute" => [$new_time['minute'], PDO::PARAM_INT],
        ], [
          "city_id" => [$city_id, PDO::PARAM_INT],
          "time_pack_id" => [$new_time['time_pack_id'], PDO::PARAM_INT],
          "month" => [$new_time['month'], PDO::PARAM_INT],
          "date" => [$new_time['date'], PDO::PARAM_INT],
        ]);
      }
    } else {
      execute_insert_sql("times", [
        "city_id" => [$city_id, PDO::PARAM_INT],
        "time_pack_id" => [$new_time['time_pack_id'], PDO::PARAM_INT],
        "month" => [$new_time['month'], PDO::PARAM_INT],
        "date" => [$new_time['date'], PDO::PARAM_INT],
        "hour" => [$new_time['hour'], PDO::PARAM_INT],
        "minute" => [$new_time['minute'], PDO::PARAM_INT],
      ]);
    }
  }
}
