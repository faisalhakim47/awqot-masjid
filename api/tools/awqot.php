<?php

function is_scheduling_active() {
  return count(explode("scheduling_compute.php", shell_exec("ps aux | grep scheduling_compute"))) > 1;
}

function get_scheduling_time() {
  $now_countdown = mktime(1, 1, 1, date("n"), date("j") + 1) - time();
  $scheduling_compute_line = shell_exec("ps aux | grep scheduling_compute.php");
  $scheduling_compute_countdown = (int) explode(" php-cgi", explode("sleep ", $scheduling_compute_line)[1])[0];
  return time() - ($scheduling_compute_countdown - $now_countdown);
}
