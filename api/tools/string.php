<?php

function random_string($length, $options = []) {
  $characters = isset($options["characters"])
  ? $options["characters"]
  : array_merge(
    range(0, 9),
    range("a", "z"),
    range("A", "Z")
  );
  $key = "";
  for ($i = 0; $i < $length; $i++) {
    $key .= $characters[array_rand($characters)];
  }
  return $key;
}
