<?php

function date_alter_hour($datestr, $change) {
  return date("Y-m-d H:i:s", strtotime($datestr) + (60 * 60 * $change));
};

function date_local($datesrc) {
  return date_alter_hour($datesrc, 7);
}
