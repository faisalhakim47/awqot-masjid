<?php

require_once __DIR__ . "/tools/database.php";
require_once __DIR__ . "/tools/time.php";

$result = execute_sql("
  SELECT hash, filename, duration
  FROM audios
")->fetchAll();

$result = array_map(function ($audio) {
  $audio["duration"] = humanize_duration($audio["duration"]);
  return $audio;
}, $result);

send_json(200, $result);
