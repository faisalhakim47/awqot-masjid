<?php

require_once __DIR__ . "/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/tools/server.php";
require_once __DIR__ . "/tools/child_process.php";
require_once __DIR__ . "/playlist_stop.php";

$audio_paths = implode(" ", array_map(function ($audio_hash) {
  return escapeshellarg(__DIR__ . "/../data/audios/" . $audio_hash);
}, explode(",", require_querystring("audio_hashes"))));

async_exec("mplayer -novideo " . $audio_paths);
