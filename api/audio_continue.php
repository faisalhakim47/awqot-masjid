<?php

require_once __DIR__ . "/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/tools/child_process.php";

$materi_player_path = __DIR__ . "/../data/materi_player";

$materi_player = exec("cat '{$materi_player_path}'");
$playing = explode(" ", $materi_player);

if (strpos($playing[3], "__PLAYING__") !== 0) {
  $audio_path = __DIR__ . "/../data/audios/" . $playing[0];
  $start_time = round(($playing[3] - $playing[2]) / 1000);
  async_exec("cvlc --play-and-exit --start-time={$start_time} {$audio_path}");
  $time_start = round(microtime(true) * 1000) - ($start_time * 1000);
  $materi_player = "{$playing[0]} {$playing[1]} {$time_start} __PLAYING__";
  exec("touch '{$materi_player_path}' && echo '{$materi_player}' > '{$materi_player_path}'");
}

echo $materi_player;
