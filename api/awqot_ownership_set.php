<?php

require_once __DIR__ . "/tools/configuration.php";

set_configuration("awqot_customer_name", $_POST["name"]);
set_configuration("awqot_customer_phonenumber", $_POST["phoneNumber"]);
set_configuration("awqot_masjid_name", $_POST["organizationName"]);
set_configuration("awqot_masjid_address", $_POST["address"]);

$masjid_photo_file = $_FILES["photo"]["tmp_name"];
$image_props = getimagesize($masjid_photo_file);
$image_type = $image_props[2];
$masjid_photo_filename = time() . image_type_to_extension($image_type);
move_uploaded_file($masjid_photo_file, __DIR__ . "/../data/" . $masjid_photo_filename);
set_configuration("awqot_masjid_photo", $masjid_photo_filename);
