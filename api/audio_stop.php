<?php

require_once __DIR__ . "/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/tools/child_process.php";

exec("killall vlc");
exec("killall mplayer");
exec("sudo killall vlc");
exec("sudo killall mplayer");

$materi_player_path = __DIR__ . "/../data/materi_player";

$materi_player = exec("cat '{$materi_player_path}'");
$playing = explode(" ", $materi_player);

if (strpos($playing[3], "__PLAYING__") === 0) {
  $time = round(microtime(true) * 1000);
  $materi_player = "{$playing[0]} {$playing[1]} {$playing[2]} {$time}";
  exec("echo '{$materi_player}' > '{$materi_player_path}'");
}

echo $materi_player;
