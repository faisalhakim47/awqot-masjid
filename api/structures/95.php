<?php
require_once __DIR__ . "/../tools/upgrade.php";

alter_structure(function () {
  execute_sql("
    ALTER TABLE time_packs
    ADD is_disabled TINYINT(1) NOT NULL DEFAULT 0
  ");
  execute_update_Sql("time_packs", [
    "is_disabled" => [1, PDO::PARAM_INT],
  ], [
    "id" => [6, PDO::PARAM_INT],
  ]);
  exec("sudo rm /etc/apache2/mods-enabled/expires.load");
  exec("sudo ln -s /etc/apache2/mods-available/expires.load /etc/apache2/mods-enabled/expires.load");
});
