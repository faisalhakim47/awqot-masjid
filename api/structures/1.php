<?php

require_once __DIR__ . "/../tools/upgrade.php";
require_once __DIR__ . "/../tools/configuration.php";

alter_structure(function () {

  create_table("provinces", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(191) NOT NULL,
    PRIMARY KEY(id)
  ");

  $provinces = json_decode(file_get_contents(__DIR__ . "/1_provinces.json"), true);

  foreach ($provinces as $province) {
    execute_insert_sql("provinces", [
      "id" => [$province["id"], PDO::PARAM_INT],
      "name" => [$province["name"], PDO::PARAM_STR],
    ]);
  }

  create_table("cities", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    province_id INT UNSIGNED NOT NULL,
    name VARCHAR(191) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(province_id) REFERENCES provinces(id)
  ");

  $cities = json_decode(file_get_contents(__DIR__ . "/1_cities.json"), true);

  foreach ($cities as $city) {
    execute_insert_sql("cities", [
      "id" => [$city["id"], PDO::PARAM_INT],
      "province_id" => [$city["province_id"], PDO::PARAM_INT],
      "name" => [$city["name"], PDO::PARAM_STR],
    ]);
  }

  create_table("time_packs", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(191) NOT NULL,
    PRIMARY KEY(id)
  ");

  execute_insert_sql("time_packs", [
    "id" => [1, PDO::PARAM_INT],
    "name" => ["Subuh", PDO::PARAM_STR],
  ]);

  execute_insert_sql("time_packs", [
    "id" => [2, PDO::PARAM_INT],
    "name" => ["Dzuhur", PDO::PARAM_STR],
  ]);

  execute_insert_sql("time_packs", [
    "id" => [3, PDO::PARAM_INT],
    "name" => ["Ashar", PDO::PARAM_STR],
  ]);

  execute_insert_sql("time_packs", [
    "id" => [4, PDO::PARAM_INT],
    "name" => ["Maghrib", PDO::PARAM_STR],
  ]);

  execute_insert_sql("time_packs", [
    "id" => [5, PDO::PARAM_INT],
    "name" => ["Isya", PDO::PARAM_STR],
  ]);

  create_table("times", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    city_id INT UNSIGNED NOT NULL,
    time_pack_id INT UNSIGNED NOT NULL,
    month TINYINT UNSIGNED NOT NULL,
    date TINYINT UNSIGNED NOT NULL,
    hour TINYINT UNSIGNED NOT NULL,
    minute TINYINT UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(city_id) REFERENCES cities(id),
    FOREIGN KEY(time_pack_id) REFERENCES time_packs(id)
  ");

  create_table("schedules", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(191) NOT NULL,
    PRIMARY KEY(id)
  ");

  create_table("schedule_times", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    schedule_id INT UNSIGNED NOT NULL,
    time_pack_id INT UNSIGNED NOT NULL,
    day TINYINT UNSIGNED NOT NULL,
    audios TEXT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(schedule_id) REFERENCES schedules(id),
    FOREIGN KEY(time_pack_id) REFERENCES time_packs(id)
  ");

  create_table("audios", "
    hash VARCHAR(191) NOT NULL,
    filename VARCHAR(191) NOT NULL,
    filetype VARCHAR(191) NOT NULL,
    duration INT UNSIGNED NOT NULL,
    PRIMARY KEY(hash)
  ");

  create_table("playlists", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(191) NOT NULL,
    audios TEXT NOT NULL,
    PRIMARY KEY(id)
  ");

  set_configuration("schedule_id", "1");
  set_configuration("city_id", "1");
  set_configuration("jumuah_time", "11:50");
  set_configuration("speaker_pins", "[17, 18, 22, 27]");
  set_configuration("jumuah_basis", "dzuhur");

});
