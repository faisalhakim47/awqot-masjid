<?php

require_once __DIR__ . "/../tools/upgrade.php";
require_once __DIR__ . "/../tools/child_process.php";

alter_structure(function () {
  execute_insert_sql("time_packs", [
    "id" => [6, PDO::PARAM_INT],
    "name" => ["Imsak", PDO::PARAM_STR],
  ]);

  execute_insert_sql("time_packs", [
    "id" => [7, PDO::PARAM_INT],
    "name" => ["Terbit", PDO::PARAM_STR],
  ]);

  execute_insert_sql("time_packs", [
    "id" => [8, PDO::PARAM_INT],
    "name" => ["Duha", PDO::PARAM_STR],
  ]);

  execute_update_sql("provinces", [
    "name" => ["KEPULAUAN BANGKA BELITUNG", PDO::PARAM_STR],
  ], [
    "id" => [3, PDO::PARAM_INT],
  ]);

  execute_update_sql("provinces", [
    "name" => ["D.I. YOGYAKARTA", PDO::PARAM_STR],
  ], [
    "id" => [34, PDO::PARAM_INT],
  ]);

  execute_insert_sql("provinces", [
    "name" => ["NUSA TENGGARA BARAT", PDO::PARAM_STR],
  ]);

  execute_insert_sql("provinces", [
    "name" => ["NUSA TENGGARA TIMUR", PDO::PARAM_STR],
  ]);

  $city_kab_ids = [2,1,3,4,5,6,7,8,9,10,11,12,13,22,21,23,474,475,476,478,479,490,488,489,491,492,496,493,494,495,498,497,500,501,502,503,504,505,439,440,441,452,451,453,456,455,355,356,357,358,359,362,363,364,365,366,232,233,234,237,238,65,66,67,70,72,75,48,49,50,51,52,54,57,458,459,464,468,469,472,470,471,473,36,33,34,35,38,37,248,250,247,246,249,251,252,253,254,255,44,45,46,47,77,76,78,79,80,81,82,83,84,85,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,507,508,511,138,139,140,141,142,143,144,145,146,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,24,26,27,28,29,30,32,176,177,178,179,183,184,186,187,188,189,190,191,198,199,202,203,204,205,207,208,210,211,212,216,215,217,223,224,225,226,227,228,229,230,231,59,62,61,64,373,374,375,376,377,378,379,385,383,384,386,388,391,392,393,395,396,410,412,411,415,413,414,419,416,417,418,422,423,399,397,398,400,401,404,403,406,407,428,429,430,438,435,436,437,367,368,371,369,370,372,260,259,261,264,265,267,266,268,269,270,272,274,271,275,273,278,279,312,315,316,317,318,319,320,321,323,324,325,326,327,328,329,330,331,332,333,335,336,337,338,339,340,341,343,346,345,347,348,351,350,352];

  foreach ($city_kab_ids as $city_id) {
    $city = execute_sql("
      SELECT cities.name AS name
      FROM cities
      WHERE cities.id = :city_id
    ", [
      ":city_id" => [$city_id, PDO::PARAM_INT],
    ])->fetch();
    execute_update_sql("cities", [
      "name" => ["KAB. " . $city["name"], PDO::PARAM_STR],
    ], [
      "id" => [$city_id, PDO::PARAM_INT],
    ]);
  }

  $city_map = [
    ["id" => 14, "name" => "KAB. GAYO LUES"],
    ["id" => 20, "name" => "KAB. NAGAN RAYA"],
    ["id" => 15, "name" => "KOTA BANDA ACEH"],
    ["id" => 477, "name" => "KAB. DELI SERDANG"],
    ["id" => 499, "name" => "KAB. PAKPAK BHARAT"],
    ["id" => 506, "name" => "KAB. TOBA SAMOSIR"],
    ["id" => 481, "name" => "KOTA GUNUNGSITOLI"],
    ["id" => 487, "name" => "KOTA TEBING TINGGI"],
    ["id" => 449, "name" => "KAB. LIMA PULUH KOTA"],
    ["id" => 450, "name" => "KAB. PADANG PARIAMAN"],
    ["id" => 454, "name" => "KAB. SIJUNJUNG"],
    ["id" => 457, "name" => "KAB. TANAH DATAR"],
    ["id" => 236, "name" => "KOTA TANJUNG PINANG"],
    ["id" => 243, "name" => "PULAU TAMBELAN KAB. BINTAN"],
    ["id" => 239, "name" => "PEKAJANG KAB. LINGGA"],
    ["id" => 242, "name" => "PULAU SERASAN KAB. NATUNA"],
    ["id" => 241, "name" => "PULAU MIDAI KAB. NATUNA"],
    ["id" => 240, "name" => "PULAU LAUT KAB. NATUNA"],
    ["id" => 71, "name" => "KAB. MUARO JAMBI"],
    ["id" => 73, "name" => "KAB. TANJUNG JABUNG BARAT"],
    ["id" => 74, "name" => "KAB. TANJUNG JABUNG TIMUR"],
    ["id" => 55, "name" => "KAB. MUKOMUKO"],
    ["id" => 56, "name" => "KAB. REJANG LEBONG"],
    ["id" => 465, "name" => "KAB. MUARA ENIM"],
    ["id" => 466, "name" => "KAB. MUSI BANYUASIN"],
    ["id" => 467, "name" => "KAB. MUSI RAWAS"],
    ["id" => null, "province_id" => 32, "name" => "KAB. MUSI RAWAS UTARA"],
    ["id" => 461, "name" => "KOTA PAGAR ALAM"],
    ["id" => 39, "name" => "KOTA PANGKAL PINANG"],
    ["id" => 257, "name" => "KAB. TULANG BAWANG"],
    ["id" => 256, "name" => "KAB. TULANG BAWANG BARAT"],
    ["id" => 258, "name" => "KAB. WAY KANAN"],
    ["id" => 244, "name" => "KOTA BANDAR LAMPUNG"],
    ["id" => null, "province_id" => 6, "name" => "KAB. KEPULAUAN SERIBU"],
    ["id" => 510, "name" => "KAB. KULON PROGO"],
    ["id" => 25, "name" => "KAB. BANGLI"],
    ["id" => null, "province_id" => 35, "name" => "KAB. BIMA"],
    ["id" => null, "province_id" => 35, "name" => "KAB. DOMPU"],
    ["id" => null, "province_id" => 35, "name" => "KAB. LOMBOK BARAT"],
    ["id" => null, "province_id" => 35, "name" => "KAB. LOMBOK TENGAH"],
    ["id" => null, "province_id" => 35, "name" => "KAB. LOMBOK TIMUR"],
    ["id" => null, "province_id" => 35, "name" => "KAB. LOMBOK UTARA"],
    ["id" => null, "province_id" => 35, "name" => "KAB. SUMBAWA"],
    ["id" => null, "province_id" => 35, "name" => "KAB. SUMBAWA BARAT"],
    ["id" => null, "province_id" => 35, "name" => "KOTA BIMA"],
    ["id" => null, "province_id" => 35, "name" => "KOTA MATARAM"],
    ["id" => null, "province_id" => 36, "name" => "KAB. ALOR"],
    ["id" => null, "province_id" => 36, "name" => "KAB. BELU"],
    ["id" => null, "province_id" => 36, "name" => "KAB. ENDE"],
    ["id" => null, "province_id" => 36, "name" => "KAB. FLORES TIMUR"],
    ["id" => null, "province_id" => 36, "name" => "KAB. KUPANG"],
    ["id" => null, "province_id" => 36, "name" => "KAB. LEMBATA"],
    ["id" => null, "province_id" => 36, "name" => "KAB. MALAKA"],
    ["id" => null, "province_id" => 36, "name" => "KAB. MANGGARAI"],
    ["id" => null, "province_id" => 36, "name" => "KAB. MANGGARAI BARAT"],
    ["id" => null, "province_id" => 36, "name" => "KAB. MANGGARAI TIMUR"],
    ["id" => null, "province_id" => 36, "name" => "KAB. NGADA"],
    ["id" => null, "province_id" => 36, "name" => "KAB. NAGEKEO"],
    ["id" => null, "province_id" => 36, "name" => "KAB. ROTE NDAO"],
    ["id" => null, "province_id" => 36, "name" => "KAB. SABU RAIJUA"],
    ["id" => null, "province_id" => 36, "name" => "KAB. SIKKA"],
    ["id" => null, "province_id" => 36, "name" => "KAB. SUMBA BARAT"],
    ["id" => null, "province_id" => 36, "name" => "KAB. SUMBA BARAT DAYA"],
    ["id" => null, "province_id" => 36, "name" => "KAB. SUMBA TENGAH"],
    ["id" => null, "province_id" => 36, "name" => "KAB. SUMBA TIMUR"],
    ["id" => null, "province_id" => 36, "name" => "KAB. TIMOR TENGAH SELATAN"],
    ["id" => null, "province_id" => 36, "name" => "KAB. TIMOR TENGAH UTARA"],
    ["id" => null, "province_id" => 36, "name" => "KOTA KUPANG"],
    ["id" => 182, "name" => "KAB. KUBU RAYA"],
    ["id" => null, "province_id" => 12, "name" => "KAB. MEMPAWAH"],
    ["id" => 192, "name" => "KAB. BARITO KUALA"],
    ["id" => 193, "name" => "KAB. HULU SUNGAI SELATAN"],
    ["id" => 194, "name" => "KAB. HULU SUNGAI TENGAH"],
    ["id" => 195, "name" => "KAB. HULU SUNGAI UTARA"],
    ["id" => 200, "name" => "KAB. TANAH BUMBU"],
    ["id" => 201, "name" => "KAB. TANAH LAUT"],
    ["id" => 206, "name" => "KAB. GUNUNG MAS"],
    ["id" => 213, "name" => "KAB. MURUNG RAYA"],
    ["id" => 214, "name" => "KAB. PULANG PISAU"],
    ["id" => null, "province_id" => 16, "name" => "KAB. BULUNGAN"],
    ["id" => null, "province_id" => 16, "name" => "KOTA TARAKAN"],
    ["id" => 60, "name" => "KAB. BONE BOLANGO"],
    ["id" => 389, "name" => "KAB. KEPULAUAN SELAYAR"],
    ["id" => 387, "name" => "KAB. PANGKAJENE DAN KEPULAUAN"],
    ["id" => 390, "name" => "KAB. SIDENRENG RAPPANG"],
    ["id" => 394, "name" => "KAB. TANA TORAJA"],
    ["id" => 382, "name" => "KOTA PAREPARE"],
    ["id" => null, "province_id" => 29, "name" => "KAB. BUTON SELATAN"],
    ["id" => null, "province_id" => 29, "name" => "KAB. BUTON TENGAH"],
    ["id" => null, "province_id" => 29, "name" => "KAB. MUNA BARAT"],
    ["id" => 405, "name" => "KAB. PARIGI MOUTONG"],
    ["id" => 408, "name" => "KAB. TOJO UNA-UNA"],
    ["id" => 409, "name" => "KAB. TOLI-TOLI"],
    ["id" => 427, "name" => "KAB. BOLAANG MONGONDOW"],
    ["id" => 424, "name" => "KAB. BOLAANG MONGONDOW SELATAN"],
    ["id" => 425, "name" => "KAB. BOLAANG MONGONDOW TIMUR"],
    ["id" => 426, "name" => "KAB. BOLAANG MONGONDOW UTARA"],
    ["id" => 277, "name" => "KOTA TIDORE KEPULAUAN"],
    ["id" => null, "province_id" => 20, "name" => "KOTA SOFIFI"],
    // ["id" => 0, "name" => "KOTA SOFIFI"],
    ["id" => 313, "name" => "KAB. BIAK NUMFOR"],
    ["id" => 314, "name" => "KAB. BOVEN DIGOEL"],
    ["id" => 334, "name" => "KAB. PUNCAK JAYA"],
    ["id" => 342, "name" => "KAB. FAKFAK"],
    ["id" => 349, "name" => "KAB. RAJA AMPAT"],
    ["id" => 353, "name" => "KAB. TELUK BINTUNI"],
    ["id" => 354, "name" => "KAB. TELUK WONDAMA"],
  ];

  foreach ($city_map as $city) {
    if ($city["id"]) {
      execute_update_sql("cities", [
        "name" => [$city["name"], PDO::PARAM_STR],
      ], [
        "id" => [$city["id"], PDO::PARAM_INT],
      ]);
    } else {
      execute_insert_sql("cities", [
        "province_id" => [$city["province_id"], PDO::PARAM_INT],
        "name" => [$city["name"], PDO::PARAM_STR],
      ]);
    }
  }

  mkdir(__DIR__ . "/../../data/schedules");
  shell_exec("tar -xf " . realpath(__DIR__ . "/8_schedules.tar.xz") . " -C " . realpath(__DIR__ . "/../../data/schedules"));

  require_once __DIR__ . "/../commands/ensure_current_times.php";
});
