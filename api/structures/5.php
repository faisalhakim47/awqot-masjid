<?php

require_once __DIR__ . "/../tools/upgrade.php";
require_once __DIR__ . "/../tools/configuration.php";

alter_structure(function () {

  set_configuration("awqot_password", "");
  set_configuration("awqot_customer_name", "");
  set_configuration("awqot_customer_phonenumber", "");
  set_configuration("awqot_masjid_address", "");

});
