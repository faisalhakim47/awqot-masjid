<?php

require_once __DIR__ . "/../tools/upgrade.php";
require_once __DIR__ . "/../tools/configuration.php";

alter_structure(function () {

  $ihtiyat = get_configuration("ihtiyat") ?: "60";

  set_configuration("ihtiyat_Subuh", $ihtiyat);
  set_configuration("ihtiyat_Dzuhur", $ihtiyat);
  set_configuration("ihtiyat_Ashar", $ihtiyat);
  set_configuration("ihtiyat_Maghrib", $ihtiyat);
  set_configuration("ihtiyat_Isya", $ihtiyat);

  create_table("schedule_changes", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    date TINYINT UNSIGNED NOT NULL,
    month TINYINT UNSIGNED NOT NULL,
    calendar VARCHAR(191) NOT NULL,
    schedule_id INT UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(schedule_id) REFERENCES schedules(id)
  ");

});
