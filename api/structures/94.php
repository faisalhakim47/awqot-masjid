<?php

require_once __DIR__ . "/../tools/upgrade.php";

alter_structure(function () {
  execute_sql("
    ALTER TABLE schedule_times
    ADD timing_basis VARCHAR(16) NOT NULL DEFAULT 'kemenag' COMMENT 'value: kemenag, manual, istiwa'
  ");
  execute_sql("
    ALTER TABLE schedule_times
    ADD timing_hour TINYINT
  ");
  execute_sql("
    ALTER TABLE schedule_times
    ADD timing_minute TINYINT
  ");
  $jumuah_basis = get_configuration("jumuah_basis");
  $jumuah_time = array_map(function ($number) {
    return (int) $number;
  }, explode(":", get_configuration("jumuah_time")));
  execute_update_sql("schedule_times", [
    "timing_basis" => [$jumuah_basis === "manual" ? "manual" : "kemenag", PDO::PARAM_STR],
    "timing_hour" => [$jumuah_time[0], PDO::PARAM_INT],
    "timing_minute" => [$jumuah_time[1], PDO::PARAM_INT],
  ], [
    "schedule_id" => [(int) get_configuration("schedule_id"), PDO::PARAM_INT],
    "time_pack_id" => [2, PDO::PARAM_INT],
    "day" => [6, PDO::PARAM_INT],
  ]);
});
 