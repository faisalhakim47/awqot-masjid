<?php
require_once __DIR__ . "/../tools/upgrade.php";
require_once __DIR__ . "/../tools/configuration.php";

alter_structure(function () {
  set_configuration("awqot_mode", "default");
});
