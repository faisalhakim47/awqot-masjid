<?php

require_once __DIR__ . "/../tools/upgrade.php";

alter_structure(function () {

  $dhcpcd_wifi = escapeshellarg(str_replace("  ", "", "
    hostname
    clientid
    persistent
    option rapid_commit
    option domain_name_servers, domain_name, domain_search, host_name
    option classless_static_routes
    option ntp_servers
    option interface_mtu
    require dhcp_server_identifier
    slaac private
    interface usb0
    static ip_address=192.168.42.42/24
    static routers=192.168.42.129
    static domain_name_servers=192.168.42.129 8.8.8.8
    interface wlan0
    static ip_address=192.168.43.43/24
    static routers=192.168.43.1
    static domain_name_servers=192.168.43.1 8.8.8.8
  "));
  exec("echo {$dhcpcd_wifi} | sudo tee /etc/dhcpcd.conf > /dev/null");

  $interfaces_wifi = escapeshellarg(str_replace("  ", "", "
    source-directory /etc/network/interfaces.d
    auto wlan0
    iface wlan0 inet manual
    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
  "));
  exec("echo {$interfaces_wifi} | sudo tee /etc/network/interfaces > /dev/null");

  $dhcpcd_hotspot = escapeshellarg(str_replace("  ", "", "
    hostname
    clientid
    persistent
    option rapid_commit
    option domain_name_servers, domain_name, domain_search, host_name
    option classless_static_routes
    option ntp_servers
    option interface_mtu
    require dhcp_server_identifier
    slaac private
    interface usb0
    static ip_address=192.168.42.42/24
    static routers=192.168.42.129
    static domain_name_servers=192.168.42.129 8.8.8.8
    interface wlan0
    static ip_address=192.168.1.1/24
    nohook wpa_supplicant
  "));
  exec("echo {$dhcpcd_hotspot} | sudo tee /etc/dhcpcd.conf > /dev/null");

  $interfaces_hotspot = escapeshellarg(str_replace("  ", "", "
    source-directory /etc/network/interfaces.d
  "));
  exec("echo {$interfaces_hotspot} | sudo tee /etc/network/interfaces > /dev/null");

});
