<?php

require_once __DIR__ . "/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/tools/server.php";
require_once __DIR__ . "/tools/child_process.php";
require_once __DIR__ . "/tools/mediainfo.php";

ob_start();
require_once __DIR__ . "/audio_stop.php";
ob_clean();

$audio_hash = require_querystring("audio_hash");
$start_time = require_querystring("start_time");
$audio_path = __DIR__ . "/../data/audios/" . $audio_hash;
$materi_player_path = __DIR__ . "/../data/materi_player";

async_exec("cvlc --play-and-exit --start-time={$start_time} {$audio_path}");
$time_start = round(microtime(true) * 1000) - ($start_time * 1000);
$duration = mediainfo($audio_path)["Duration"];
$materi_player = "{$audio_hash} {$duration} {$time_start} __PLAYING__";
exec("touch '{$materi_player_path}' && echo '{$materi_player}' > '{$materi_player_path}'");
echo $materi_player;
