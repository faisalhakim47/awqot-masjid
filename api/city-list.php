<?php

require_once __DIR__ . "/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/tools/database.php";

$province_id = require_querystring("province_id");

$result = execute_sql("
  SELECT
    cities.id AS id,
    cities.name AS name
  FROM cities
  WHERE cities.province_id = :province_id
", [
  ":province_id" => [$province_id, PDO::PARAM_INT],
])->fetchAll();

send_json(200, $result);
