#!/usr/bin/env php
<?php

require_once __DIR__ . "/../tools/child_process.php";
require_once __DIR__ . "/../tools/configuration.php";
require_once __DIR__ . "/../tools/sholat_schedule.php";

$city_id = (int) get_configuration("city_id");

ensure_sholat_schedule($city_id);

async_exec("php-cgi " . realpath(__DIR__ . "/scheduling_compute.php"));
