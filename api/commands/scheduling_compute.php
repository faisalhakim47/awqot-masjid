<?php

require_once __DIR__ . "/../commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../tools/child_process.php";
require_once __DIR__ . "/../tools/configuration.php";
require_once __DIR__ . "/../tools/database.php";
require_once __DIR__ . "/../tools/raspberry.php";

(function () {
  $app_dir = realpath(__DIR__ . "/../..");

  // ---------- NETRALIZE ----------

  require __DIR__ . "/scheduling_stop.php";
  async_exec("php-cgi {$app_dir}/api/commands/speaker_off.php");
  exec("sudo hwclock -s");

  // ---------- SET AUDIO CHANNEL ----------

  $awqot_mode = get_configuration("awqot_mode");
  $audio_output_mode = $awqot_mode === "mini"
    ? "2" // HDMI
    : "0"; // AUTO
  exec("sudo raspi-config nonint do_audio " . $audio_output_mode);

  // ---------- SCHEDULE CHANGER ----------

  require_once __DIR__ . "/schedule_changer.php";

  // ---------- MODEL ----------

  $ihtiyats = array_map(function ($time_pack) {
    return [
      "time_pack_id" => $time_pack["id"],
      "second" => (int) get_configuration("ihtiyat_{$time_pack["name"]}"),
    ];
  }, execute_sql("
    SELECT id, name
    FROM time_packs
    WHERE is_disabled = 0
    ORDER BY id ASC
  ")->fetchAll());

  $timezone_city_id = (int) get_configuration("city_id");

  $active_schedule_id = (int) get_configuration("schedule_id");

  $istiwa_time_diff = (int) get_configuration("istiwa_time_diff", 0);

  $time = round(microtime(true) * 1000);
  $month = date("n");
  $date = date("j");
  $day = date("w") + 1;

  $schedule_times = execute_sql("
    SELECT
      schedule_times.time_pack_id AS time_pack_id,
      schedule_times.day AS day,
      schedule_times.audios AS audios,
      schedule_times.timing_basis AS timing_basis,
      schedule_times.timing_hour AS timing_hour,
      schedule_times.timing_minute AS timing_minute
    FROM schedule_times
    WHERE schedule_times.schedule_id = :schedule_id
      AND schedule_times.day = :day
  ", [
    ":schedule_id" => [$active_schedule_id, PDO::PARAM_INT],
    ":day" => [$day, PDO::PARAM_INT],
  ])->fetchAll();

  $scheduled_times = [];

  foreach ($schedule_times as $schedule_time) {
    $audio_hashes = json_decode($schedule_time["audios"], true);

    $audios = array_map(function ($audio_hash) {
      return execute_sql("
        SELECT
          audios.hash AS hash,
          audios.duration AS duration
        FROM audios
        WHERE audios.hash = :audio_hash
      ", [
        ":audio_hash" => [$audio_hash, PDO::PARAM_STR],
      ])->fetch();
    }, $audio_hashes);

    $total_duration = 0;

    foreach ($audios as $audio) {
      $total_duration += $audio["duration"];
    }

    if ($schedule_time["timing_basis"] === "manual") {
      $audio_end_time = mktime(
        (int) $schedule_time["timing_hour"],
        (int) $schedule_time["timing_minute"],
        0
      ) * 1000;
    } else if ($schedule_time["timing_basis"] === "istiwa") {
      $audio_end_time = (mktime(
        (int) $schedule_time["time_hour"],
        (int) $schedule_time["time_minute"],
        0
      ) + $istiwa_time_diff) * 1000;
    } else if ($schedule_time["timing_basis"] === "kemenag") {
      $data_time = execute_sql("
        SELECT
          times.hour AS hour,
          times.minute AS minute
        FROM times
        WHERE times.month = :month
          AND times.date = :date
          AND times.time_pack_id = :time_pack_id
          AND times.city_id = :city_id
      ", [
        ":month" => [$month, PDO::PARAM_INT],
        ":date" => [$date, PDO::PARAM_INT],
        ":time_pack_id" => [$schedule_time["time_pack_id"], PDO::PARAM_INT],
        ":city_id" => [$timezone_city_id, PDO::PARAM_INT],
      ])->fetch();
      $audio_end_time = mktime(
        $data_time["hour"],
        $data_time["minute"],
        0
      ) * 1000;
      foreach ($ihtiyats as $ihtiyat) {
        if ($ihtiyat["time_pack_id"] === $schedule_time["time_pack_id"]) {
          $audio_end_time += $ihtiyat["second"] * 1000;
          break;
        }
      }
    }

    $audio_begin_time = $audio_end_time - $total_duration;

    $speaker_begin_time = $audio_begin_time - 5000;
    $speaker_end_time = $audio_end_time + 2500;

    // NO CONTENT
    if ($total_duration === 0) {
      continue;
    }

    array_push($scheduled_times, [
      "time_pack_id" => $schedule_time["time_pack_id"],
      "timing_basis" => $schedule_time["timing_basis"],
      "audios" => $audio_hashes,
      "audio_begin_time" => $audio_begin_time,
      "total_duration" => $total_duration,
    ]);

    // PAST
    if ($audio_end_time <= $time) {
      continue;
    }

    // FUTURE
    else if ($audio_begin_time > $time) {
      $audio_begin_countdown = ($audio_begin_time - $time) / 1000;
      $audio_paths = implode(" ", array_map(function ($audio_hash) use ($app_dir) {
        return "{$app_dir}/data/audios/{$audio_hash}";
      }, $audio_hashes));
      $speaker_begin_countdown = ($speaker_begin_time - $time) / 1000;
      $speaker_begin_countdown = $speaker_begin_countdown < 0 ? 0 : $speaker_begin_countdown;

      async_exec("sleep {$speaker_begin_countdown} && php-cgi {$app_dir}/api/commands/speaker_on.php");
      async_exec("sleep {$audio_begin_countdown} && mplayer {$audio_paths}");
    }

    // PRESENT
    else if ($audio_begin_time <= $time && $audio_end_time > $time) {
      $audio_skipped_time = $time - $audio_begin_time;
      $audio_paths = array_map(function ($audio_hash) use ($app_dir) {
        return "{$app_dir}/data/audios/{$audio_hash}";
      }, $audio_hashes);

      $first_audio = "";
      $next_audios = [];
      $next_audios_delay = 0;
      foreach ($audio_paths as $audio_path) {
        if ($first_audio === "") {
          $duration = (int) exec("mediainfo --Inform='Audio;%Duration%' '{$audio_path}'");
          if ($duration < $audio_skipped_time) {
            $audio_skipped_time = $audio_skipped_time - $duration;
            continue;
          }
          $first_audio = $audio_path;
          $next_audios_delay = $duration - $audio_skipped_time;
        } else {
          array_push($next_audios, $audio_path);
        }
      }
      $audio_skipped_time = $audio_skipped_time / 1000;
      $next_audios_delay = $next_audios_delay / 1000;
      $next_audios_string = implode(" ", $next_audios);
      async_exec("php-cgi {$app_dir}/api/commands/speaker_on.php");
      async_exec("cvlc --play-and-exit --start-time={$audio_skipped_time} {$first_audio}");
      if (count($next_audios) !== 0) {
        async_exec("sleep {$next_audios_delay} && mplayer {$next_audios_string}");
      }
    }

    $speaker_end_countdown = ($speaker_end_time - $time) / 1000;
    async_exec("sleep {$speaker_end_countdown} && php-cgi {$app_dir}/api/commands/speaker_off.php");

    // Memastikan schedule_changer dieksekusi
    async_exec("sleep {$speaker_end_countdown} && php-cgi {$app_dir}/api/commands/scheduling_compute.php");
  }

  $recompute_countdown = mktime(1, 1, 1, date("n"), date("j") + 1) - time();
  async_exec("sleep {$recompute_countdown} && php-cgi {$app_dir}/api/commands/scheduling_compute.php");

  file_put_contents($app_dir . "/data/scheduled_times.json", json_encode($scheduled_times));
})();
