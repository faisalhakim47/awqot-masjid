<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/raspberry.php";

// ----- CONTROLLER -----

if (isset($_POST["action"])) switch ($_POST["action"]) {
  case "volume_set":
  exec("amixer sset Master {$_POST["volume"]}%");
  exit();
  break;

  case "speaker_on":
  speaker_on();
  exit();
  break;

  case "speaker_off":
  speaker_off();
  exit();
  break;

  case "audio_stop":
  require_once __DIR__ . "/../api/audio_stop.php";
  exit();
  break;
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  header("Location: {$_SERVER["REQUEST_URI"]}");
  exit();
}

// ----- MODEL -----

$volume = substr(explode(" ", exec("amixer sget Master"))[6], 1, -2);

$speaker_status = true;

// ----- VIEW -----

require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/box.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_group.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/sidebar.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Speaker - Awqot</title>
  <?php publish("head"); ?>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 3.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <span class="icon">
              <?php include __DIR__ . "/../static/icons/round-menu-24px.svg"; ?>
            </span>
          </button>
          <h1 class="page-title">Speaker</h1>
        </div>
      </div>
    </header>

    <main class="page-content padded">
      <div class="box">
        <div class="box-content">
          <div class="field">
            <div class="field-label">
              <label for="volume_input">Volume</label>
            </div>
            <div class="field-input">
              <input
                id="volume_input"
                type="range"
                min="0"
                max="100"
                value="<?= $volume ?>"
                onchange="changeVolume(this.value);"
              >
            </div>
            <button type="button" class="button outline" style="width: 50%; white-space: nowrap;" onclick="audioStop();">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-stop-24px.svg" ?>
              </span>
              <span class="text">Hentikan Audio</span>
            </button>
          </div>
          <div class="field">
            <div class="field-label">
              <label>Speaker</label>
            </div>
            <div class="field-input">
              <div class="button-group justify-around">
                <button type="button" class="button outline" style="width: 50%;" onclick="speakerOn();">
                  <span class="icon">
                    <?php include __DIR__ . "/../static/icons/round-volume_up-24px.svg" ?>
                  </span>
                  <span class="text">Hidupkan</span>
                </button>
                <button type="button" class="button outline" style="width: 50%;" onclick="speakerOff();">
                  <span class="icon">
                    <?php include __DIR__ . "/../static/icons/round-volume_off-24px.svg" ?>
                  </span>
                  <span class="text">Matikan</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>

  <script>
    document.addEventListener("DOMContentLoaded", function () {
      window.changeVolume = function changeVolume(newVolume) {
        var request = new XMLHttpRequest();
        var data = new FormData();
        data.append("action", "volume_set");
        data.append("volume", newVolume);
        request.open("POST", "./speaker.php", true);
        request.send(data);
      }

      window.audioStop = function audioStop() {
        var request = new XMLHttpRequest();
        request.open('GET', '/api/audio_stop.php');
        request.send();
      }

      window.speakerOn = function speakerOn() {
        var request = new XMLHttpRequest();
        request.open('GET', '/api/commands/speaker_on.php');
        request.send();
      }

      window.speakerOff = function speakerOff() {
        var request = new XMLHttpRequest();
        request.open('GET', '/api/commands/speaker_off.php');
        request.send();
      }
    });
  </script>
</body>

</html>
