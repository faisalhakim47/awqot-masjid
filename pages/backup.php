<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/database.php";
require_once __DIR__ . "/../api/tools/child_process.php";
require_once __DIR__ . "/../api/tools/configuration.php";
require_once __DIR__ . "/../api/tools/timezone.php";

// ----- CONTEXT -----

$BACKUP_VERSION = 1;

$storage_total_kib = (int) exec("df | grep /dev/root | awk '{print $2}'");
$storage_available_kib = (int) exec("df | grep /dev/root | awk '{print $4}'");
$storage_usage_kib = $storage_total_kib - $storage_available_kib;
$storage_total_gib = $storage_total_kib / pow(1024, 2);
$storage_available_gib = $storage_available_kib / pow(1024, 2);
$storage_usage_gib = $storage_usage_kib / pow(1024, 2);

$open_backup_items_does_not_exist_dialog = false;
$open_backup_ready_dialog = false;

$backup_items = (isset($_POST["backup_items"]) && is_array($_POST["backup_items"]))
  ? $_POST["backup_items"]
  : [];

if (!empty($_POST["schedule_ids"])) {
  array_push($backup_items, "schedules");
}

if (!empty($_POST["playlist_ids"])) {
  array_push($backup_items, "playlists");
}

// ----- CONTROLLER -----

if (isset($_POST["action"])) switch ($_POST["action"]) {
  case "prepare_backup":
    if (count($backup_items) === 0) {
      $open_backup_items_does_not_exist_dialog = true;
      break;
    }
    $audio_hashes = [];
    foreach ($backup_items as $backup_item) switch ($backup_item) {
      case "schedules":
        foreach ($_POST["schedule_ids"] as $schedule_id) {
          $schedule_times = execute_sql("
            SELECT audios
            FROM schedule_times
            WHERE schedule_id = :schedule_id
          ", [
            ":schedule_id" => [$schedule_id, PDO::PARAM_INT],
          ])->fetchAll();
          foreach ($schedule_times as $schedule_time) {
            $schedule_time_audio_hashes = json_decode($schedule_time["audios"]);
            if (is_array($schedule_time_audio_hashes)) foreach ($schedule_time_audio_hashes as $audio_hash) {
              array_push($audio_hashes, $audio_hash);
            }
          }
        }
        break;
      case "playlists":
        foreach ($_POST["playlist_ids"] as $playlist_id) {
          $playlist = execute_sql("
            SELECT audios
            FROM playlists
            WHERE id = :playlist_id
            LIMIT 1
          ", [
            ":playlist_id" => [$playlist_id, PDO::PARAM_INT],
          ])->fetch();
          $playlist_audio_hashes = json_decode($playlist["audios"]);
          if (is_array($playlist_audio_hashes)) foreach ($playlist_audio_hashes as $audio_hash) {
            array_push($audio_hashes, $audio_hash);
          }
        }
        break;
    }
    $unique_audio_hashes = array_unique($audio_hashes, SORT_STRING);
    $audio_hashes_total_bytes = array_reduce(array_map(function ($audio_hash) {
      $audio_path = __DIR__ . "/../data/audios/" . $audio_hash;
      return file_exists($audio_path) ? filesize($audio_path) : 0;
    }, $unique_audio_hashes), function ($sum, $bytes) {
      return $sum + $bytes;
    }, 0);
    $audio_hashes_total_bytes += 1 * pow(1024, 2); // Database size approx.
    $audio_hashes_total_mib = $audio_hashes_total_bytes / pow(1024, 2);
    $open_backup_ready_dialog = true;
    break;

  case "do_backup":
    $audio_hashes = [];
    $schedules = [];
    $schedule_times = [];
    $schedule_changes= [];
    $timezone_configurations = [];
    $playlists = [];
    foreach ($backup_items as $backup_item) switch ($backup_item) {
      case "schedules":
        foreach ($_POST["schedule_ids"] as $schedule_id) {
          $schedule = execute_sql("
            SELECT *
            FROM schedules
            WHERE id = :schedule_id
          ", [
            ":schedule_id" => [$schedule_id, PDO::PARAM_INT],
          ])->fetch();
          $_schedule_times = execute_sql("
            SELECT *
            FROM schedule_times
            WHERE schedule_id = :schedule_id
          ", [
            ":schedule_id" => [$schedule_id, PDO::PARAM_INT],
          ])->fetchAll();
          foreach ($_schedule_times as $schedule_time) {
            $schedule_time_audio_hashes = json_decode($schedule_time["audios"]);
            if (is_array($schedule_time_audio_hashes)) foreach ($schedule_time_audio_hashes as $audio_hash) {
              array_push($audio_hashes, $audio_hash);
            }
            array_push($schedule_times, $schedule_time);
          }
          array_push($schedules, $schedule);
        }
        break;
      case "schedule_changes":
        foreach ($_POST["schedule_ids"] as $schedule_id) {
          $_schedule_changes = execute_sql("
            SELECT *
            FROM schedule_changes
            WHERE schedule_id = :schedule_id
          ", [
            ":schedule_id" => [$schedule_id, PDO::PARAM_INT],
          ])->fetchAll();
          foreach ($_schedule_changes as $schedule_change) {
            array_push($schedule_changes, $schedule_change);
          }
        }
        break;
      case "timezone_configurations":
        $ihtiyats = execute_sql("
          SELECT *
          FROM configurations
          WHERE name LIKE 'ihtiyat_%'
        ")->fetchAll();
        foreach ($ihtiyats as $ihtiyat) {
          array_push($timezone_configurations, $ihtiyat);
        }
        array_push($timezone_configurations, [
          "name" => "city_id",
          "value" => get_configuration("city_id"),
        ]);
        array_push($timezone_configurations, [
          "name" => "timezone",
          "value" => get_timezone(),
        ]);
        break;
      case "playlists":
        foreach ($_POST["playlist_ids"] as $playlist_id) {
          $playlist = execute_sql("
            SELECT *
            FROM playlists
            WHERE id = :playlist_id
            LIMIT 1
          ", [
            ":playlist_id" => [$playlist_id, PDO::PARAM_INT],
          ])->fetch();
          $playlist_audio_hashes = json_decode($playlist["audios"]);
          if (is_array($playlist_audio_hashes)) foreach ($playlist_audio_hashes as $audio_hash) {
            array_push($audio_hashes, $audio_hash);
          }
          array_push($playlists, $playlist);
        }
        break;
    }
    $audios = array_map(function ($audio_hash) {
      return execute_sql("SELECT * FROM audios WHERE `hash` = :audio_hash LIMIT 1", [
        ":audio_hash" => [$audio_hash, PDO::PARAM_STR],
      ])->fetch();
    }, $audio_hashes);
    
    // Prepare destination
    if (!is_dir(__DIR__ . "/../data/backups")) mkdir(__DIR__ . "/../data/backups");
    exec("sudo rm -rf " . escapeshellarg(__DIR__ . "/../data/backups/preparation_data.json"));

    $data_dir = __DIR__ . "/../data/backups/";
    $data_filename = "data.json";

    file_put_contents($data_dir . $data_filename, json_encode([
      "BACKUP_VERSION" => $BACKUP_VERSION,
      "audios" => $audios,
      "schedules" => $schedules,
      "schedule_times" => $schedule_times,
      "schedule_changes" => $schedule_changes,
      "timezone_configurations" => $timezone_configurations,
      "playlists" => $playlists,
    ]));

    $backup_filename = time() . ".tar.gz";

    exec(
      "tar -czf "
      . escapeshellarg(__DIR__ . "/../data/backups/" . $backup_filename) . " "
      . "-C " . $data_dir . " " . $data_filename . " "
      . "-C " . __DIR__ . "/../data/audios/ " . implode(" ", array_map(function ($audio_hash) {
        return escapeshellarg($audio_hash);
      }, $audio_hashes))
    );

    unlink($data_path);

    header("Location: /pages/backup.php?new_backup_filename=" . $backup_filename);
    exit();

  case "delete_backup":
    unlink(__DIR__ . "/../data/backups/" . $_POST["filename"]);
    break;

  case "prepare_import":
    $file = $_FILES["backup"];
    $import_preparation_dir = __DIR__ . "/../data/import_preparation";
    exec("sudo rm -rf " . $import_preparation_dir);
    mkdir($import_preparation_dir);
    move_uploaded_file($file["tmp_name"], $import_preparation_dir . "/data.tar.gz");
    exec(
      "tar -xzf "
      . escapeshellarg($import_preparation_dir . "/data.tar.gz") . " "
      . "-C " . escapeshellarg($import_preparation_dir)
    );
    unlink($import_preparation_dir . "/data.tar.gz");
    header("Location: /pages/backup.php?import_ready=" . $file["name"]);
    exit();

  case "do_import":
    if (empty($_POST["submit_type"])) {
      break;
    }
    if ($_POST["submit_type"] === "cancel") {
      shell_exec("sudo rm -rf " . __DIR__ . "/../data/import_preparation/*");
    } else if ($_POST["submit_type"] === "import") {
      $base = __DIR__ . "/../data/import_preparation";
      if (!file_exists($base . "/data.json")) {
        break;
      }
      $data = json_decode(file_get_contents($base . "/data.json"), true);
      foreach ($data["audios"] as $audio) {
        $audio_exists = execute_sql("SELECT * FROM audios WHERE `hash` = :audio_hash", [
          ":audio_hash" => $audio["hash"],
        ])->fetch();
        if (!$audio_exists) {
          execute_insert_sql("audios", $audio);
          file_put_contents(__DIR__ . "/../data/audios/" . $audio_hash, file_get_contents($base . "/" . $audio["hash"]));
        }
      }
      $schedule_map = [];
      foreach ($data["schedules"] as $schedule) {
        $backup_schedule_id = $schedule["id"];
        unset($schedule["id"]);
        $name_exists = execute_sql("SELECT * FROM schedules WHERE `name` = :schedule_name", [
          ":schedule_name" => [$schedule["name"], PDO::PARAM_STR],
        ]);
        if ($name_exists) {
          $schedule["name"] = $schedule["name"] . " [BARU]";
        }
        $schedule_id = execute_insert_sql("schedules", $schedule);
        $schedule_map[$backup_schedule_id] = $schedule_id;
      }
      foreach ($data["schedule_times"] as $schedule_time) {
        unset($schedule_time["id"]);
        $schedule_time["schedule_id"] = $schedule_map[$schedule_time["schedule_id"]];
        execute_insert_sql("schedule_times", $schedule_time);
      }
      foreach ($data["schedule_changes"] as $schedule_change) {
        unset($schedule_change["id"]);
        $schedule_change["schedule_id"] = $schedule_map[$schedule_change["schedule_id"]];
        execute_insert_sql("schedule_changes", $schedule_change);
      }
      foreach ($data["playlists"] as $playlist) {
        unset($playlist["id"]);
        $name_exists = execute_sql("SELECT * FROM playlists WHERE `name` = :playlist_name", [
          ":playlist_name" => [$playlist["name"], PDO::PARAM_STR],
        ]);
        if ($name_exists) {
          $playlist["name"] = $playlist["name"] . " [BARU]";
        }
        execute_insert_sql("playlists", $playlist);
      }
      foreach ($data["timezone_configurations"] as $configuration) {
        if ($configuration["name"] === "timezone") {
          set_timezone($configuration["value"]);
        } else {
          set_configuration($configuration["name"], $configuration["value"]);
        }
      }
      shell_exec("sudo rm -rf " . __DIR__ . "/../data/import_preparation/*");
    }
    break;
}

// ----- MODEL -----

$schedules = execute_sql("
  SELECT id, name
  FROM schedules
")->fetchAll();

$playlists = execute_sql("
  SELECT id, name
  FROM playlists
")->fetchAll(); 

$backup_files = array_map(
  function ($line) {
    $parts = explode(" ", $line);
    $size_bytes = (int) $parts[4];
    $filename = $parts[9];
    $created_at = (int) explode(".", $filename)[0];
    return [
      "filename" => $parts[9],
      "label" => date("d M Y H:i", $created_at),
      "size_bytes" => $size_bytes,
      "size_mib" => (int) ceil($size_bytes / pow(1024, 2)),
    ];
  },
  array_values(
    array_filter(
      explode("\n", shell_exec("ls -la " . escapeshellarg(__DIR__ . "/../data/backups"))),
      function ($line) {
        return strpos($line, "tar.gz") !== false;
      }
    )
  )
);

// ----- DATA -----

$open_import_ready_dialog = file_exists(__DIR__ . "/../data/import_preparation/data.json");
$import_preparation = $open_import_ready_dialog
  ? json_decode(file_get_contents(__DIR__ . "/../data/import_preparation/data.json"), true)
  : false;

// ----- VIEW -----

require_once __DIR__ . "/../components/box.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_fab.php";
require_once __DIR__ . "/../components/button_group.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/sidebar.php";
require_once __DIR__ . "/../components/table.php";
require_once __DIR__ . "/../components/modal.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Import dan Export - Awqot</title>
  <?php publish("head"); ?>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 3.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <span class="icon">
              <?php include __DIR__ . "/../static/icons/round-menu-24px.svg"; ?>
            </span>
          </button>
          <h1 class="page-title">Import dan Export</h1>
        </div>
      </div>
    </header>

    <main class="page-content padded">
      <p>
        <span style="display: flex; justify-content: space-between; align-items: flex-end;">
          <span style="font-size: 1.25rem;">Penyimpanan</span>
          <span>Tersedia <?= number_format($storage_available_gib, 2, ',', '.') ?>GiB</span>
        </span>
        <meter
          min="0"
          max="<?= $storage_total_kib ?>"
          high="<?= $storage_total_kib * 88/100 ?>"
          value="<?= $storage_usage_kib ?>"
          style="display: block; width: 100%; height: 1.5rem;"
        ></meter>
        <span style="display: flex; justify-content: space-between;">
          <span>Terpakai <?= number_format($storage_usage_gib, 2, ',', '.') ?>GiB</span>
          <span>Total <?= number_format($storage_total_gib, 2, ',', '.') ?>GiB</span>
        </span>
      </p>
      <section class="box">
        <div class="box-content">
          <table class="table">
            <thead>
              <tr>
                <th colspan="3">Backup untuk Di-export</th>
              </tr>
            </thead>
            <?php if (count($backup_files) === 0): ?>
            <tbody>
              <tr>
                <td colspan="3" style="padding: 1rem;">
                  <p style="box-sizing: border-box; display: block; border-radius: .25rem; width: 100%; margin: 0px; padding: 1rem; background-color: #efefef; font-style: italic;">belum ada backup.</p>
                </td>
              </tr>
            </tbody>
            <?php else: ?>
            <tbody>
              <?php foreach ($backup_files as $file): ?>
              <tr>
                <td style="padding-right: .5rem; width: 100%;"><?= explode(".", $file["label"])[0] ?></td>
                <td style="padding: 0px .5rem;"><?= $file["size_mib"] ?>MiB</td>
                <td style="padding-right: 0px; width: 80px; max-width: 80px;">
                  <div class="button-group" style="max-width: 80px;">
                    <button
                      type="button"
                      class="button danger"
                      onclick="
                        backup_delete.querySelector('input[name=filename]').value = '<?= $file['filename'] ?>';
                        ev.publish('modal:open', { dialog: backup_delete });
                      "
                    >
                      <span class="icon">
                        <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
                      </span>
                    </button>
                    <a class="button" href="/data/backups/<?= $file["filename"] ?>" download="awqot_backup_<?= $file["filename"] ?>" target="_blank">
                      <span class="icon">
                        <?php include __DIR__ . "/../static/icons/round-download-24px.svg" ?>
                      </span>
                    </a>
                  </div>
                </td>
              </tr>
              <?php endforeach ?>
            </tbody>
            <?php endif ?>
          </table>
        </div>
      </section>
      <button
        type="button"
        class="button-fab secondary"
        onclick="
          ev.publish('modal:open', { dialog: prepare_import_dialog });
        "
      >
        <span class="icon">
          <?php include __DIR__ . "/../static/icons/round-publish-24px.svg" ?>
        </span>
        <span class="text">Import</span>
      </button>
      <button type="button" class="button-fab" onclick="ev.publish('modal:open', { dialog: backup_dialog })">
        <span class="icon">
          <?php include __DIR__ . "/../static/icons/round-library_add-24px.svg" ?>
        </span>
        <span class="text">Backup Baru</span>
      </button>
    </main>

    <dialog id="backup_dialog" class="modal">
      <form class="modal-shell" method="post" style="height: 100%;">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Buat Backup</h4>
            </div>
          </div>
        </header>
        <div class="modal-content" style="height: 100%; border-top: 1px solid #CCCCCC; border-bottom: 1px solid #CCCCCC;">
          <input type="hidden" name="action" value="prepare_backup">
          <ul style="list-style: none; padding-left: 1rem; margin: 0px 0px 1rem 0px;">
            <li>
              <span style="display: flex; align-items: center; height: 2.5rem; margin-top: .5rem; font-weight: 500;">Paket Materi</span>
              <ul style="list-style: none; padding-left: 1.5rem; margin: 0px 0px 1rem 0px;">
                <?php foreach ($schedules as $schedule): ?>
                <li>
                  <label style="height: 2.5rem; display: flex; align-items: center;">
                    <input type="checkbox" name="schedule_ids[]" value="<?= $schedule["id"] ?>">
                    <span style="display: inline-block; padding-left: .25rem;"><?= $schedule["name"] ?></span>
                  </label>
                </li>
                <?php endforeach ?>
              </ul>
            </li>
            <li>
              <span style="display: flex; align-items: center; height: 2.5rem; margin-top: .5rem; font-weight: 500;">Playlist</span>
              <ul style="list-style: none; padding-left: 1.5rem; margin: 0px 0px 1rem 0px;">
                <?php foreach ($playlists as $playlist): ?>
                <li>
                  <label style="height: 2.5rem; display: flex; align-items: center;">
                    <input type="checkbox" name="playlist_ids[]" value="<?= $playlist["id"] ?>">
                    <span style="display: inline-block; padding-left: .25rem;"><?= $playlist["name"] ?></span>
                  </label>
                </li>
                <?php endforeach ?>
              </ul>
            </li>
            <li style="height: 2.5rem;">
              <label style="height: 2.5rem; display: flex; align-items: center;">
                <input type="checkbox" name="backup_items[]" value="schedule_changes">
                <span style="display: inline-block; padding-left: .25rem;">Perubahan Paket Otomatis</span>
              </label>
            </li>
            <li style="height: 2.5rem;">
              <label style="height: 2.5rem; display: flex; align-items: center;">
                <input type="checkbox" name="backup_items[]" value="timezone_configurations">
                <span style="display: inline-block; padding-left: .25rem;">Konfigurasi Waktu dan Wilayah</span>
              </label>
            </li>
          </ul>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: backup_dialog });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Siapkan Backup</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>

    <dialog id="backup_delete" class="modal">
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <div class="modal-content">
          <input type="hidden" name="action" value="delete_backup">
          <input type="hidden" name="filename">
          <p>Apakah anda yakin menghapus backup ini?</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: backup_delete });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button danger">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>

    <dialog id="prepare_import_dialog" class="modal">
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post" enctype="multipart/form-data">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Persiapan Import</h4>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="prepare_import">
          <div class="field">
            <div class="field-label">
              <label for="backup_file_input">File Backup</label>
            </div>
            <div class="field-input">
              <input
                id="backup_file_input"
                type="file"
                name="backup"
                accept="application/gzip"
                required
              >
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: prepare_import_dialog });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Upload</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
  </div>

  <?php if ($open_backup_items_does_not_exist_dialog): ?>
    <dialog id="backup_items_does_not_exist_dialog" class="modal">
      <form class="modal-shell" method="post">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Kesalahan Input</h4>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <p style="margin-top: 0px;">Anda belum memilih data untuk dibackup.</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left"></div>
          <div class="modal-footer-right">
            <button type="button" class="button" onclick="ev.publish('modal:close', { dialog: backup_items_does_not_exist_dialog })">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Tutup</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <script>
      ev.publish('modal:open', { dialog: backup_items_does_not_exist_dialog })
    </script>
  <?php endif ?>

  <?php if ($open_backup_ready_dialog): ?>
    <dialog id="backup_ready_dialog" class="modal">
      <form
        class="modal-shell"
        method="post"
        onsubmit="
          this.querySelector('.button.primary').disabled = true;
          this.querySelector('.button.primary .text').textContent = 'Memproses...';
          this.querySelector('.button.secondary').style.display = 'none';
        "
      >
        <input type="hidden" name="action" value="do_backup">
        <?php foreach ($_POST["backup_items"] as $backup_item): ?>
          <input type="hidden" name="backup_items[]" value="<?= $backup_item ?>">
        <?php endforeach ?>
        <?php if (!empty($_POST["schedule_ids"])) foreach ($_POST["schedule_ids"] as $schedule_id): ?>
          <input type="hidden" name="schedule_ids[]" value="<?= $schedule_id ?>">
        <?php endforeach ?>
        <?php if (!empty($_POST["playlist_ids"])) foreach ($_POST["playlist_ids"] as $playlist_id): ?>
          <input type="hidden" name="playlist_ids[]" value="<?= $playlist_id ?>">
        <?php endforeach ?>
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Data Siap Di-backup</h4>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <p style="margin-top: 0px;">Anda membutuhkan penyimpanan sebesar <strong><?= ceil($audio_hashes_total_mib)  ?>MiB</strong> untuk menyimpan backup.</p>
          <p>Backup membutuhkan waktu beberapa menit. Harap tunggu sampai proses selesai!</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: backup_ready_dialog });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Buat Backup</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <script>
      ev.publish('modal:open', { dialog: backup_ready_dialog })
    </script>
  <?php endif ?>

  <?php if ($open_import_ready_dialog): ?>
    <dialog id="import_ready_dialog" class="modal">
      <form
        class="modal-shell"
        method="post"
        style="height: 100%;"
        onsubmit="
          setTimeout(function(its) {
            its.querySelector('.button.danger').disabled = true;
            its.querySelector('.button.primary').disabled = true;
          }, 0, this);
        "
      >
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Pilih Data Import</h4>
            </div>
          </div>
        </header>
        <div class="modal-content" style="height: 100%; border-top: 1px solid #CCCCCC; border-bottom: 1px solid #CCCCCC;">
          <p>Apabila ada nama yang sama, maka akan diberi imbuhan <em>[BARU]</em> diakhir.</p>
          <input type="hidden" name="action" value="do_import">
          <ul style="list-style: none; padding-left: 1rem; margin: 0px 0px 1rem 0px;">
            <?php if (!empty($import_preparation["schedules"])): ?>
              <li>
                <span style="display: flex; align-items: center; height: 2.5rem; margin-top: .5rem; font-weight: 500;">Paket Materi</span>
                <ul style="list-style: none; padding-left: 1.5rem; margin: 0px 0px 1rem 0px;">
                  <?php foreach ($import_preparation["schedules"] as $schedule): ?>
                    <li>
                      <label style="height: 2.5rem; display: flex; align-items: center;">
                        <input type="checkbox" name="schedule_ids[]" value="<?= $schedule["id"] ?>" checked>
                        <span style="display: inline-block; padding-left: .25rem;"><?= $schedule["name"] ?></span>
                      </label>
                    </li>
                  <?php endforeach ?>
                </ul>
              </li>
            <?php endif ?>
            <?php if (!empty($import_preparation["playlists"])): ?>
              <li>
                <span style="display: flex; align-items: center; height: 2.5rem; margin-top: .5rem; font-weight: 500;">Playlist</span>
                <ul style="list-style: none; padding-left: 1.5rem; margin: 0px 0px 1rem 0px;">
                  <?php foreach ($import_preparation["playlists"] as $playlist): ?>
                    <li>
                      <label style="height: 2.5rem; display: flex; align-items: center;">
                        <input type="checkbox" name="playlist_ids[]" value="<?= $playlist["id"] ?>" checked>
                        <span style="display: inline-block; padding-left: .25rem;"><?= $playlist["name"] ?></span>
                      </label>
                    </li>
                  <?php endforeach ?>
                </ul>
              </li>
            <?php endif ?>
            <?php if (!empty($import_preparation["schedule_changes"])): ?>
              <li style="height: 2.5rem;">
                <label style="height: 2.5rem; display: flex; align-items: center;">
                  <input type="checkbox" name="backup_items[]" value="schedule_changes" checked>
                  <span style="display: inline-block; padding-left: .25rem;">Perubahan Paket Otomatis</span>
                </label>
              </li>
            <?php endif ?>
            <?php if (!empty($import_preparation["timezone_configurations"])): ?>
              <li style="height: 2.5rem;">
                <label style="height: 2.5rem; display: flex; align-items: center;">
                  <input type="checkbox" name="backup_items[]" value="timezone_configurations" checked>
                  <span style="display: inline-block; padding-left: .25rem;">Konfigurasi Waktu dan Wilayah</span>
                </label>
              </li>
            <?php endif ?>
          </ul>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="submit"
              class="button danger"
              name="submit_type"
              value="cancel"
              onclick="this.querySelector('.text').textContent = 'Membatalkan...'"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Batalkan</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button
              type="submit"
              class="button primary"
              name="submit_type"
              value="import"
              onclick="this.querySelector('.text').textContent = 'Memproses...'"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Import</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <script>
      ev.publish('modal:open', { dialog: import_ready_dialog })
    </script>
  <?php endif ?>
</body>

</html>