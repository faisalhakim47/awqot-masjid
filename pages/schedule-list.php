<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/database.php";

// ----- CONTROLLER -----

if (isset($_POST["action"]) && $_POST["action"] === "create_schedule") {
  $schedule_id = execute_insert_sql("schedules", [
    "name" => [$_POST["name"], PDO::PARAM_STR],
  ]);
  header("Location: /pages/schedule.php?schedule_id={$schedule_id}");
  exit();
}

else if (isset($_POST["action"]) && $_POST["action"] === "delete_schedule") {
  $schedule_id = $_POST["schedule_id"];
  execute_delete_sql("schedule_times", [
    "schedule_id" => [$schedule_id, PDO::PARAM_INT],
  ]);
  execute_delete_sql("schedules", [
    "id" => [$schedule_id, PDO::PARAM_INT],
  ]);
  header("Location: /pages/schedule-list.php");
}

else if (isset($_POST["action"]) && $_POST["action"] === "activate_schedule") {
  set_configuration("schedule_id", $_POST["schedule_id"]);
}

else if (isset($_POST["action"]) && $_POST["action"] === "schedule_duplicate") {
  $schedule_times = execute_sql("
    SELECT
      schedule_times.day AS day,
      schedule_times.time_pack_id AS time_pack_id,
      schedule_times.audios AS audios
    FROM schedule_times
    WHERE schedule_times.schedule_id = :schedule_id
  ", [
    ":schedule_id" => [$_POST["schedule_id"], PDO::PARAM_INT],
  ])->fetchALl();
  $schedule_id = execute_insert_sql("schedules", [
    "name" => [$_POST["name"], PDO::PARAM_STR],
  ]);
  foreach ($schedule_times as $schedule_time) {
    execute_insert_sql("schedule_times", [
      "schedule_id" => [$schedule_id, PDO::PARAM_INT],
      "day" => [$schedule_time["day"], PDO::PARAM_INT],
      "time_pack_id" => [$schedule_time["time_pack_id"], PDO::PARAM_INT],
      "audios" => [$schedule_time["audios"], PDO::PARAM_STR],
    ]);
  }
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  require_once __DIR__ . "/../api/commands/scheduling_compute.php";
  header("Location: {$_SERVER["REQUEST_URI"]}");
  exit();
}

// ----- MODEL -----

$schedules = execute_sql("
  SELECT
    schedules.id AS id,
    schedules.name AS name
  FROM schedules
  ORDER BY schedules.name ASC
")->fetchAll();

$active_schedule_id = (int) get_configuration("schedule_id");

// ----- VIEW -----

require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/box.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_fab.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/list_view.php";
require_once __DIR__ . "/../components/menu.php";
require_once __DIR__ . "/../components/modal.php";
require_once __DIR__ . "/../components/notification.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/sidebar.php";
require_once __DIR__ . "/../components/tag.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Paket Materi - Awqot</title>
  <?php publish("head"); ?>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 3.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <div class="icon">
              <?php include __DIR__ . "/../static/icons/round-menu-24px.svg" ?>
            </div>
          </button>
          <h1 class="page-title">Paket Materi</h1>
        </div>
      </div>
    </header>

    <main class="page-content">
      <?php if (count($schedules) === 0): ?>
      <p
        class="notification"
        onclick="ev.publish('modal:open', { dialog: schedule_create });"
      >Anda belum memiliki paket materi. Klik disini atau tombol tambah hijau di pojok bawah untuk membuat paket materi.</p>
      <?php endif ?>
      <ul class="list-view">
        <?php foreach ($schedules as $schedule): ?>
        <li
          class="list-view-item"
          data-schedule_id="<?= $schedule["id"] ?>"
        >
          <div class="list-view-item-row">
            <a
              class="list-view-item-title"
              href="/pages/schedule.php?schedule_id=<?= $schedule["id"] ?>"
            ><?= $schedule["name"] ?></a>
            <?php if ($active_schedule_id === $schedule["id"]): ?>
            <span class="tag">aktif</span>
            <?php else: ?>
            <form  method="post">
              <input type="hidden" name="action" value="activate_schedule">
              <input type="hidden" name="schedule_id" value="<?= $schedule["id"]?>">
              <button
                type="submit"
                class="button"
                <?= $active_schedule_id === $schedule["id"] ? "disabled" : "" ?>
                style="width: 100%;"
              >
                <span class="text">Aktifkan</span>
              </button>
            </form>
            <?php endif ?>
            <button
              type="button"
              class="button"
              onclick="openMenu(<?= $schedule['id'] ?>)"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-more_vert-24px.svg" ?>
              </span>
            </button>
            <div class="menu" style="display: none; opacity: 0;">
              <ul class="menu-list">
                <li class="menu-item">
                  <a class="button" href="/pages/schedule.php?schedule_id=<?= $schedule["id"] ?>">
                    <span class="icon">
                      <?php include __DIR__ . "/../static/icons/round-edit-24px.svg" ?>
                    </span>
                    <span class="text">Edit</span>
                  </a>
                </li>
                <li class="menu-item">
                  <button
                    type="button"
                    class="button"
                    onclick="openScheduleDuplicateModal(<?= $schedule['id'] ?>)"
                  >
                    <span class="icon">
                      <?php include __DIR__ . "/../static/icons/round-file_copy-24px.svg" ?>
                    </span>
                    <span class="text">Duplikat</span>
                  </button>
                </li>
                <li class="menu-item">
                  <button
                    type="button"
                    class="button"
                    onclick="openScheduleDeleteModal(<?= $schedule['id'] ?>)"
                  >
                    <span class="icon">
                      <?php include __DIR__ . "/../static/icons/outline-delete_forever-24px.svg" ?>
                    </span>
                    <span class="text">Hapus</span>
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </li>
        <?php endforeach ?>
      </ul>
      <div style="display: flex; justify-content: center; padding: .5rem 0;">
        <a href="/pages/schedule_change-list.php" class="button primary outline">
          <span class="icon">
            <?php include __DIR__ . "/../static/icons/round-brightness_auto-24px.svg" ?>
          </span>
          <span class="text">Perubahan Paket Otomatis</span>
        </a>
      </div>
    </main>

    <button type="button" class="button-fab" onclick="ev.publish('modal:open', { dialog: schedule_create });">
      <span class="icon">
        <?php include __DIR__ . "/../static/icons/round-add-24px.svg" ?>
      </span>
      <span class="text">Paket Baru</span>
    </button>

    <!-- SCHEDULE_PACK -->
    <dialog id="schedule_create" class="modal">
      <form class="modal-shell" method="post">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Buat Paket Jadwal</h4>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="create_schedule">
          <div class="field">
            <div class="field-label">
              <label class="label">Nama Paket</label>
            </div>
            <div class="field-input">
              <input
                ref="schedule_name_input"
                class="input"
                type="text"
                name="name"
                placeholder="ketik disini..."
                required
              >
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: schedule_create });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Simpan</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /SCHEDULE_PACK -->

    <!-- SCHEDULE_PACK_DUPLICATE -->
    <dialog id="schedule_duplicate" class="modal">
      <form class="modal-shell" method="post">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title"></h4>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="schedule_duplicate">
          <input type="hidden" name="schedule_id">
          <div class="field">
            <div class="field-label">
              <label class="label">Nama Paket Baru</label>
            </div>
            <div class="field-input">
              <input
                class="input"
                type="text"
                name="name"
                placeholder="ketik disini..."
                required
              >
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: schedule_duplicate });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Duplikat</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /SCHEDULE_PACK_DUPLICATE -->

    <!-- SCHEDULE_PACK_DELETE -->
    <dialog id="schedule_delete" class="modal">
      <form class="modal-shell" method="post">
        <div class="modal-content">
          <input type="hidden" name="action" value="delete_schedule">
          <input type="hidden" name="schedule_id">
          <p>Apakah anda yakin menghapus paket "<span data-schedule-name></span>"?</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: schedule_delete });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button danger">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /SCHEDULE_PACK_DELETE -->

  </div>

  <script>
    function openMenu(scheduleId) {
      var scheduleEl = document.querySelector('[data-schedule_id="' + scheduleId + '"]');
      var menuEl = scheduleEl.querySelector('.menu');
      if (menuEl.classList.contains('active')) return;
      menuEl.classList.add('active');
      ev.publish('animate', {
        duration: 150,
        transition: function fadeIn(progress) {
          if (progress === 0) menuEl.style.display = 'block';
          menuEl.style.opacity = progress;
        },
      });
      var clickEveryWhere = function clickEveryWhere(event) {
        removeEventListener('click', clickEveryWhere);
        ev.publish('animate', {
          duration: 150,
          transition: function fadeOut(progress) {
            menuEl.style.opacity = 1 - progress;
            if (progress === 1) menuEl.classList.remove('active');
          },
        });
      };
      setTimeout(function () {
        addEventListener('click', clickEveryWhere);
      });
    }
    function openScheduleDuplicateModal(scheduleId) {
      var schedule_duplicate = document.getElementById('schedule_duplicate');
      requestAnimationFrame(function () {
        schedule_duplicate.querySelector('[name="schedule_id"]').value = scheduleId;
        schedule_duplicate.querySelector('.modal-title').textContent = 'Duplikat Paket Jadwal: '
          + document.querySelector('[data-schedule_id="' + scheduleId + '"]')
            .querySelector('.list-view-item-title')
            .textContent;
      });
      ev.publish('modal:open', { dialog: schedule_duplicate });
    }
    function openScheduleDeleteModal(scheduleId) {
      var schedule_delete = document.getElementById('schedule_delete');
      requestAnimationFrame(function () {
        schedule_delete.querySelector('[name="schedule_id"]').value = scheduleId;
        schedule_delete.querySelector('[data-schedule-name]').textContent =
          document.querySelector('[data-schedule_id="' + scheduleId + '"]')
            .querySelector('.list-view-item-title')
            .textContent;
      });
      ev.publish('modal:open', { dialog: schedule_delete });
    }
  </script>
</body>

</html>
