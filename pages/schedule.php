<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/database.php";
require_once __DIR__ . "/../api/tools/javascript.php";
require_once __DIR__ . "/../api/tools/time.php";
require_once __DIR__ . "/../api/tools/timezone.php";

// ----- CONTEXT -----

$schedule_id = (int) require_querystring("schedule_id");

$day_id = (int) $_GET["day_id"];
$today_id = date('w') + 1;
if ($day_id === 0) {
  header("Location: {$_SERVER["REQUEST_URI"]}&day_id={$today_id}");
  exit();
}

$time_pack_id = (int) $_GET["time_pack_id"];
if ($time_pack_id === 0) {
  $time_pack_id = execute_sql("
    SELECT id, name
    FROM time_packs
    WHERE is_disabled = 0
    ORDER BY id ASC
    LIMIT 1
  ")->fetch()["id"];
  header("Location: {$_SERVER["REQUEST_URI"]}&time_pack_id={$time_pack_id}");
  exit();
}

$audios = isset($_POST["audios"])
  ? $_POST["audios"]
  : [];

$time_packs = execute_sql("
  SELECT id, name
  FROM time_packs
  WHERE is_disabled = 0
  ORDER BY id ASC
")->fetchAll();

$days = [
  [ "value" => "ahad", "label" => "Ahad" ],
  [ "value" => "senin", "label" => "Senin" ],
  [ "value" => "selasa", "label" => "Selasa" ],
  [ "value" => "rabu", "label" => "Rabu" ],
  [ "value" => "kamis", "label" => "Kamis" ],
  [ "value" => "jumat", "label" => "Jum'at" ],
  [ "value" => "sabtu", "label" => "Sabtu" ],
];
for ($index = 0; $index < count($days); $index++) {
  $days[$index] = [
    "id" => $index + 1,
    "value" => $days[$index]["value"],
    "label" => $days[$index]["label"],
  ];
}

$active_day = $days[0];
foreach ($days as $day) {
  if ($day["id"] === $day_id) {
    $active_day = $day;
    break;
  }
}

// ----- CONTROLLER -----

if (isset($_POST["action"])) switch ($_POST["action"]) {
  case "update_schedule":
  execute_update_sql("schedules", [
    "name" => [$_POST["name"], PDO::PARAM_STR],
  ], [
    "id" => [$schedule_id, PDO::PARAM_INT],
  ]);
  break;

  case "delete_schedule":
  execute_delete_sql("schedule_times", [
    "schedule_id" => [$schedule_id, PDO::PARAM_INT],
  ]);
  execute_delete_sql("schedules", [
    "id" => [$schedule_id, PDO::PARAM_INT],
  ]);
  header("Location: /pages/schedule-list.php");
  break;

  case "add_schedule_time_audios":
  $schedule_time_id = $_POST["schedule_time_id"];
  $new_audios = array_filter($_POST["audio_hashes"], function ($audio) {
    return $audio !== "";
  });
  $saved_audios = execute_sql("
    SELECT audios
    FROM schedule_times
    WHERE id = :schedule_time_id
  ", [
    ":schedule_time_id" => [$schedule_time_id, PDO::PARAM_INT],
  ])->fetch();
  $saved_audios = json_decode($saved_audios["audios"], true) ?: [];
  foreach ($new_audios as $new_audio) {
    array_push($saved_audios, $new_audio);
  }
  execute_update_sql("schedule_times", [
    "audios" => [json_encode($saved_audios), PDO::PARAM_STR],
  ], [
    "id" => [$schedule_time_id, PDO::PARAM_INT],
  ]);
  break;

  case "set_schedule_time_audios":
  $schedule_time_id = $_POST["schedule_time_id"];
  $audio_hashes = $_POST["audio_hashes"];
  execute_update_sql("schedule_times", [
    "audios" => [json_encode($audio_hashes), PDO::PARAM_STR],
  ], [
    "id" => [$schedule_time_id, PDO::PARAM_INT],
  ]);
  break;

  case "set_timing_basis":
  $timing_parts = explode(":", $_POST["timing_time"]);
  $timing_hour = (int) $timing_parts[0];
  $timing_minute = (int) $timing_parts[1];
  execute_update_sql("schedule_times", [
    "timing_basis" => [$_POST["timing_basis"], PDO::PARAM_STR],
    "timing_hour" => [$timing_hour, PDO::PARAM_INT],
    "timing_minute" => [$timing_minute, PDO::PARAM_INT],
  ], [
    "schedule_id" => [$schedule_id, PDO::PARAM_INT],
    "time_pack_id" => [$time_pack_id, PDO::PARAM_INT],
    "day" => [$day_id, PDO::PARAM_INT],
  ]);
  break;

  case "delete_schedule_time":
  execute_delete_sql("schedule_times", [
    "id" => [$_POST["id"], PDO::PARAM_INT],
  ]);
  break;

  case "schedule_copy":
  $source_time_pack_ids = isset($_POST["source_time_pack_all"]) && $_POST["source_time_pack_all"] === "yes"
    ? "all"
    : array_map(function ($str) {
      return (int) $str;
    }, $_POST["source_time_pack_ids"]);
  $source_timing_basiss = isset($_POST["source_timing_basis_all"]) && $_POST["source_timing_basis_all"] === "yes"
    ? "all"
    : array_map(function ($str) {
      return (int) $str;
    }, $_POST["source_timing_basiss"]);
  $target_day_ids = isset($_POST["target_day_all"]) && $_POST["target_day_all"] === "yes"
    ? array_map(function ($day) {
      return $day["id"];
    }, $days)
    : array_map(function ($str) {
      return (int) $str;
    }, $_POST["target_day_ids"]);
  foreach ($time_packs as $time_pack) {
    $time_pack_id = (int) $time_pack["id"];
    $source_schedule_time = null;
    /**
     * Duplikat Materi
     */
    if ($source_time_pack_ids === "all" || in_array($time_pack_id, $source_time_pack_ids)) {
      $source_schedule_time = execute_sql("
        SELECT audios, timing_basis, timing_hour, timing_minute
        FROM schedule_times
        WHERE schedule_id = :schedule_id
          AND day = :day
          AND time_pack_id = :time_pack_id
      ", [
        ":schedule_id" => [$schedule_id, PDO::PARAM_INT],
        ":day" => [$day_id, PDO::PARAM_INT],
        ":time_pack_id" => [$time_pack_id, PDO::PARAM_INT],
      ])->fetch();
      foreach ($target_day_ids as $target_day_id) {
        execute_update_sql("schedule_times", [
          "audios" => $source_schedule_time["audios"],
        ], [
          "schedule_id" => $schedule_id,
          "day" => $target_day_id,
          "time_pack_id" => $time_pack_id,
        ]);
      }
    }
    /**
     * Duplikat Basis Waktu
     */
    if ($source_timing_basiss === "all" || in_array($time_pack_id, $source_timing_basiss)) {
      if (is_null($source_schedule_time)) {
        $source_schedule_time = execute_sql("
          SELECT audios, timing_basis, timing_hour, timing_minute
          FROM schedule_times
          WHERE schedule_id = :schedule_id
            AND day = :day
            AND time_pack_id = :time_pack_id
        ", [
          ":schedule_id" => [$schedule_id, PDO::PARAM_INT],
          ":day" => [$day_id, PDO::PARAM_INT],
          ":time_pack_id" => [$time_pack_id, PDO::PARAM_INT],
        ])->fetch();
      }
      foreach ($target_day_ids as $target_day_id) {
        execute_update_sql("schedule_times", [
          "timing_basis" => $source_schedule_time["timing_basis"],
          "timing_hour" => $source_schedule_time["timing_hour"],
          "timing_minute" => $source_schedule_time["timing_minute"],
        ], [
          "schedule_id" => $schedule_id,
          "day" => $target_day_id,
          "time_pack_id" => $time_pack_id,
        ]);
      }
    }
  }
  break;
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  require_once __DIR__ . "/../api/commands/scheduling_compute.php";
  header("Location: {$_SERVER["REQUEST_URI"]}");
  exit();
}

// ----- MODEL -----

$schedule = execute_sql("
  SELECT
    schedules.id AS id,
    schedules.name AS name
  FROM schedules
  WHERE schedules.id = :schedule_id
", [
  ":schedule_id" => [$schedule_id, PDO::PARAM_INT],
])->fetch();

if ($schedule === false) {
  header("Location: /pages/schedule-list.php");
}

$schedule_time = execute_sql("
  SELECT
    schedule_times.id AS id,
    schedule_times.audios AS audios,
    schedule_times.timing_basis AS timing_basis,
    schedule_times.timing_hour AS timing_hour,
    schedule_times.timing_minute AS timing_minute
  FROM schedule_times
  WHERE
    schedule_times.schedule_id = :schedule_id
    AND schedule_times.time_pack_id = :time_pack_id
    AND schedule_times.day = :day_id
  ORDER BY time_pack_id ASC
  LIMIT 1
", [
  ":schedule_id" => [$schedule_id, PDO::PARAM_INT],
  ":day_id" => [$day_id, PDO::PARAM_INT],
  ":time_pack_id" => [$time_pack_id, PDO::PARAM_INT],
])->fetch();

if (!$schedule_time) {
  $schedule_time = execute_insert_sql("schedule_times", [
    "schedule_id" => [$schedule_id, PDO::PARAM_INT],
    "time_pack_id" => [$time_pack_id, PDO::PARAM_INT],
    "day" => [$day_id, PDO::PARAM_INT],
    "audios" => ["[]", PDO::PARAM_STR],
  ]);
  $schedule_time = [
    "id" => $schedule_time,
    "audios" => "[]",
    "timing_basis" => "kemenag",
    "timing_hour" => null,
    "timing_minute" => null,
  ];
}

$schedule_timing_time = str_pad($schedule_time["timing_hour"], 2, "0", STR_PAD_LEFT) . ":" . str_pad($schedule_time["timing_minute"], 2, "0", STR_PAD_LEFT);

$schedule_time_audios = array_map(function ($audio_hash) {
  return execute_sql("
    SELECT
      audios.hash AS hash,
      audios.filename AS filename,
      audios.filetype AS filetype,
      audios.duration AS duration
    FROM audios
    WHERE audios.hash = :audio_hash
  ", [
    ":audio_hash" => [$audio_hash, PDO::PARAM_STR],
  ])->fetch();
}, json_decode($schedule_time["audios"], true));

$timezone = get_timezone();

if ($schedule_time["timing_basis"] === "kemenag") {
  $city_id = get_configuration("city_id");
  $city = execute_sql("
    SELECT
      cities.province_id AS province_id,
      cities.name AS name
    FROM cities
    WHERE cities.id = :city_id
  ", [
    ":city_id" => [$city_id, PDO::PARAM_INT],
  ])->fetch();

  $province = execute_sql("
    SELECT provinces.name AS name
    FROM provinces
    WHERE provinces.id = :province_id
  ", [
    ":province_id" => [$city["province_id"], PDO::PARAM_INT],
  ])->fetch();
}

$audios = execute_sql("
  SELECT
    audios.hash,
    audios.filename,
    audios.duration
  FROM audios
  ORDER BY audios.filename
")->fetchAll();

// ----- VIEW -----

require_once __DIR__ . "/../components/animation.php";
require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_fab.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/list_view.php";
require_once __DIR__ . "/../components/materi_player.php";
require_once __DIR__ . "/../components/materi_selector.php";
require_once __DIR__ . "/../components/modal.php";
require_once __DIR__ . "/../components/notification.php";
require_once __DIR__ . "/../components/page.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title><?= $schedule["name"] ?> – Awqot Masjid</title>
  <?php publish("head"); ?>
  <style>
.schedule-controls {
  background-color: #FFFFFF;
  position: fixed;
  border-radius: .4rem;
  right: 1.5rem;
  top: 40%;
  top: calc(50% - 4.5rem);
  transform: translateX(10rem);
  transition-duration: 300ms;
  transition-property: transform;
  box-shadow: 0 8px 17px 2px rgba(0,0,0,0.14),
    0 3px 14px 2px rgba(0,0,0,0.12),
    0 5px 5px -3px rgba(0,0,0,0.2);
}
.audio-selected .schedule-controls {
  transform: translateX(0);
}
.schedule-controls .button {
  min-height: 3.5rem;
  border-radius: .4rem;
}
.schedule-controls .button:hover {
  background-color: rgba(0, 0, 0, .05);
}
.schedule-controls .button .icon {
  padding: 1.25rem 1rem;
  margin: 0 !important;
}
.schedule-controls .button:disabled {
  cursor: not-allowed; 
}
.schedule-controls .button:disabled:hover {
  cursor: not-allowed; 
  background-color: rgba(0, 0, 0, 0);
}
.schedule-controls .button:disabled .icon svg path:last-child {
  fill: #aaa;
}

.button-fab.secondary {
  transition-duration: 300ms;
  transition-property: transform;
}

.time-pack-mode-container {
  display: flex;
  justify-content: space-between;
  align-items: center;
  max-width: 640px;
  padding: .5rem 0rem;
  margin: .5rem;
  border-radius: .5rem;
  border: 1px solid #ccc;
  background-color: #fff;
}

.button-text {
  font-weight: 500;
  padding: 0px .75rem;
  color: #0277BD;
}

@media (min-width: 640px) {
  .time-pack-mode-container {
    margin: .5rem auto;
  }
}
  </style>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 9.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <a class="button" href="/pages/schedule-list.php">
            <span class="icon">
              <?php include __DIR__ . "/../static/icons/round-chevron_left-24px.svg"; ?>
            </span>
          </a>
          <h1 class="page-title"><?= $schedule["name"] ?></h1>
        </div>
        <div class="page-header-right">
          <button type="button" class="button" onclick="ev.publish('modal:open', { dialog: schedule_pack });">
            <span class="icon">
              <?php include __DIR__ . "/../static/icons/round-edit-24px.svg"; ?>
            </span>
          </button>
        </div>
      </div>
      <nav class="page-header-row">
        <ul class="page-header-tab primary">
          <?php foreach ($days as $day): ?>
          <li
            class="
              page-header-tab-item
              <?= $day["id"] === $day_id ? "activated" : "" ?>
              <?= $day["id"] === ($day_id - 1) ? "is-preceding-active" : "" ?>
            "
          >
            <a href="?schedule_id=<?= $schedule_id ?>&day_id=<?= $day["id"] ?>&time_pack_id=<?= $time_pack_id ?>">
              <?= $day["label"] ?>
            </a>
          </li>
          <?php endforeach; ?>
        </ul>
        <script>document.querySelector('.page-header-tab-item.activated').scrollIntoView();</script>
        <ul class="page-header-tab secondary">
          <?php foreach ($time_packs as $time_pack): ?>
            <?php if (in_array((int) $time_pack["id"], [6, 7, 8])): ?>
              <li class="page-header-tab-item page-header-tab-item-secondary <?= $time_pack["id"] === $time_pack_id ? "activated" : "" ?>">
                  <a
                    href="?schedule_id=<?= $schedule_id ?>&day_id=<?= $day_id ?>&time_pack_id=<?= $time_pack["id"] ?>"
                  ><?= $time_pack["name"] ?></a>
              </li>
            <?php else: ?>
              <li class="page-header-tab-item <?= $time_pack["id"] === $time_pack_id ? "activated" : "" ?>">
                  <a
                    href="?schedule_id=<?= $schedule_id ?>&day_id=<?= $day_id ?>&time_pack_id=<?= $time_pack["id"] ?>"
                  ><?= $time_pack["name"] ?></a>
              </li>
            <?php endif ?>
          <?php endforeach; ?>
        </ul>
      </nav>
    </header>

    <main class="page-content" style="padding-bottom: 10rem;">
      <?php if (count($schedule_time_audios) === 0): ?>
      <p class="notification" onclick="ev.publish('materi_selector:open')">belum ada materi. Klik disini atau tombol tambah di pojok bawah untuk manambahkan materi.</p>
      <?php else: ?>
      <?php if ($schedule_time["timing_basis"] === "kemenag"): ?>
      <div class="time-pack-mode-container">
        <p style="margin: 0px; padding: 0px .75rem;">
          <span style="font-size: .96rem; font-weight: 500;">Basis Waktu</span>
          <br><span style="font-style: italic;">Mengikuti jadwal Kemenag</span>
        </p>
        <button
          type="button"
          class="button button-text"
          onclick="ev.publish('modal:open', { dialog: timing_basis_dialog });"
        >Ubah</button>
      </div>
      <?php elseif ($schedule_time["timing_basis"] === "manual"): ?>
      <div class="time-pack-mode-container">
        <p style="margin: 0px; padding: 0px .75rem;">
          <span style="font-size: .96rem; font-weight: 500;">Basis Waktu</span>
          <br><span style="font-style: italic;">Ditentukan sendiri. Pukul: <?= $schedule_timing_time ?> <?= $timezone ?></span>
        </p>
        <button
          type="button"
          class="button button-text"
          onclick="ev.publish('modal:open', { dialog: timing_basis_dialog });"
        >Ubah</button>
      </div>
      <?php elseif ($schedule_time["timing_basis"] === "istiwa"): ?>
      <!--
      <div class="time-pack-mode-container">
        <p style="margin: 0px; padding: 0px .75rem;">Pukul <?= $schedule_timing_time ?> waktu istiwa setempat</p>
        <button
          type="button"
          class="button button-text"
          onclick="ev.publish('modal:open', { dialog: timing_basis_dialog });"
        >Ubah</button>
      </div>
      -->
      <?php endif?>
      <ul class="list-view selectable">
        <?php $total_duration = 0 ?>
        <?php foreach ($schedule_time_audios as $schedule_time_audio): ?>
        <?php $total_duration += $schedule_time_audio["duration"]; ?>
        <li
          class="list-view-item audio-item"
          onclick="ev.publish('schedule:audio_toggle_select', { item: this });"
          data-hash="<?= $schedule_time_audio["hash"] ?>"
        >
          <div class="list-view-item-row">
            <span
              class="duration"
              style="font-size: .9rem; color: #424242; padding-left: .75rem;"
            ><?= humanize_duration($schedule_time_audio["duration"]) ?></span>
            <span class="list-view-item-title">
              <span class="flex-ellipsis"><?= $schedule_time_audio["filename"] ?></span>
            </span>
          </div>
        </li>
        <?php endforeach ?>
      </ul>
      <?php if (count($schedule_time_audios) > 1): ?>
      <p
        style="
          box-sizing: border-box;
          max-width: 640px;
          margin: 0 auto;
          padding: .75rem .5rem;
          background-color: #EEE;
          color: #424242;
        "
      >
        <span>total durasi:</span>
        <strong
          style="display: inline-block; margin-left: .25rem; font-weight: 500;"
        ><?= humanize_duration($total_duration); ?></strong>
      </p>
      <?php endif ?>
      <form class="schedule-controls">
        <button
          type="button"
          class="button position-control"
          onclick="ev.publish('schedule:set_audio_position', { direction: 'up' });"
        >
          <div class="icon">
            <?php include __DIR__ . "/../static/icons/round-arrow_upward-24px.svg" ?>
          </div>
        </button>
        <button type="button" class="button primary" onclick="openAudioMateriPlayer();">
          <div class="icon">
            <?php include __DIR__ . "/../static/icons/round-play_arrow-24px.svg" ?>
          </div>
        </button>
        <button type="button" class="button danger" onclick="ev.publish('schedule:open_audio_delete_modal')">
          <div class="icon">
            <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
          </div>
        </button>
        <button
          type="button"
          class="button position-control"
          onclick="ev.publish('schedule:set_audio_position', { direction: 'down' });"
        >
          <div class="icon">
            <?php include __DIR__ . "/../static/icons/round-arrow_downward-24px.svg" ?>
          </div>
        </button>
      </form>
      <?php endif ?>
    </main>

    <button type="button" class="button-fab secondary" onclick="ev.publish('modal:open', { dialog: schedule_copy_dialog })">
      <span class="icon">
        <?php include __DIR__ . "/../static/icons/round-content_copy-24px.svg" ?>
      </span>
      <span class="text">Duplikat</span>
    </button>

    <button type="button" class="button-fab" onclick="ev.publish('materi_selector:open')">
      <span class="icon">
        <?php include __DIR__ . "/../static/icons/round-library_add-24px.svg" ?>
      </span>
      <span class="text">Materi Baru</span>
    </button>

    <!-- timing_basis -->
    <dialog id="timing_basis_dialog" class="modal">
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <div class="modal-content">
          <input type="hidden" name="action" value="set_timing_basis">
          <div class="field">
            <div class="field-label">
              <label for="timing_basis_input">Basis Waktu</label>
            </div>
            <div class="field-input">
              <select
                id="timing_basis_input"
                name="timing_basis"
                onchange="ev.publish('schedule:timing_basis_select_change')"
              >
                <option
                  value="kemenag"
                  <?= $schedule_time["timing_basis"] === "kemenag" ? "selected" : "" ?>
                >Mengikuti jadwal Kemenag</option>
                <option
                  value="manual"
                  <?= $schedule_time["timing_basis"] === "manual" ? "selected" : "" ?>
                >Tentukan sendiri</option>
                <!--
                <option
                  value="istiwa"
                  <?= $schedule_time["timing_basis"] === "istiwa" ? "selected" : "" ?>
                >Tentukan sendiri (istiwa)</option>
                -->
              </select>
            </div>
            <p id="timing_basis_info" class="field-info">Ubah wilayah jadwal Kemenag melalui menu <strong>Waktu dan Wilayah</strong>.</p>
          </div>
          <div
            id="timing_manual_time_field"
            class="field"
            <?php if ($schedule_time["timing_basis"] !== "manual"): ?>
            style="display: none";
            <?php endif ?>
          >
            <div class="field-label">
              <label for="timing_manual_time_input">Waktu Materi Berakhir (<?= $timezone ?>):</label>
            </div>
            <div class="field-input">
              <input
                id="timing_manual_time_input"
                type="time"
                name="timing_time"
                <?php if ($schedule_time["timing_basis"] === "manual"): ?>
                value="<?= $schedule_timing_time ?>"
                <?php endif ?>
              >
            </div>
          </div>
          <!--
          <div
            id="timing_istiwa_time_field"
            class="field"
            <?php if ($schedule_time["timing_basis"] !== "istiwa"): ?>
            style="display: none";
            <?php endif ?>
          >
            <div class="field-label">
              <label for="timing_istiwa_time_input">Waktu Materi Berakhir (WIS):</label>
            </div>
            <div class="field-input">
              <input
                id="timing_istiwa_time_input"
                type="time"
                name="timing_time"
                <?php if ($schedule_time["timing_basis"] === "istiwa"): ?>
                value="<?= $schedule_timing_time ?>"
                <?php endif ?>
              >
            </div>
          </div>
          -->
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="
                ev.publish('modal:close', { dialog: timing_basis_dialog });
                timing_basis_input.value = '<?= $schedule_time['timing_basis'] ?>';
                ev.publish('schedule:timing_basis_select_change');
              "
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Simpan</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /timing_basis -->

    <!-- SCHEDULE_PACK -->
    <dialog id="schedule_pack" class="modal">
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Atur Paket Jadwal</h4>
            </div>
            <div class="modal-header-right">
              <button
                type="button"
                class="button secondary"
                onclick="ev.publish('modal:close', { dialog: schedule_pack });"
              >
                <span class="icon">
                  <?php include __DIR__ . "/../static/icons/round-close-24px.svg"; ?>
                </span>
              </button>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="update_schedule">
          <div class="field">
            <div class="field-label">
              <label class="label">Nama Paket</label>
            </div>
            <div class="field-input">
              <input
                ref="schedule_name_input"
                class="input"
                type="text"
                name="name"
                value="<?= $schedule["name"] ?>"
                placeholder="ketik disini..."
                required
              >
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button danger"
              onclick="ev.publish('modal:open', { dialog: schedule_pack_delete });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus Paket</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Perbarui</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /SCHEDULE_PACK -->

    <!-- SCHEDULE_PACK_DELETE -->
    <dialog id="schedule_pack_delete" class="modal">
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <div class="modal-content">
          <input type="hidden" name="action" value="delete_schedule">
          <p>Apakah anda yakin menghapus paket ini?</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: schedule_pack_delete });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button danger">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /SCHEDULE_PACK_DELETE -->

    <!-- AUDIOS_DELETE -->
    <dialog id="audios_delete" class="modal">
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <div class="modal-content">
          <input type="hidden" name="action" value="set_schedule_time_audios">
          <input type="hidden" name="schedule_time_id" value="<?= $schedule_time["id"] ?>">
          <p>Apakah anda yakin menghapus materi-materi yang terpilih?</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('schedule:close_audio_delete_modal')"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button danger">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /AUDIOS_DELETE -->

    <!-- SCHEDULE_COPY_DIALOG -->
    <dialog id="schedule_copy_dialog" class="modal">
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post" onsubmit="this.querySelector('button[type=submit]').disabled = true;">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Duplikat Hari <?= $active_day["label"] ?></h4>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="schedule_copy">
          <input type="hidden" name="schedule_time_id" value="<?= $schedule_time["id"] ?>">
          <div style="display: flex;">
            <div style="width: 33.333%; padding: .75rem; list-style: none; background-color: #efefef; border-top-right-radius: 0; border-bottom-right-radius: 0;">
              <p style="margin-top: 0px; margin-bottom: .5rem;">
                <label>
                  <input
                    type="checkbox"
                    name="source_time_pack_all"
                    value="yes"
                    onchange="ev.publish('schedule:toogle_select_all', {
                      condition: this.checked,
                      name: 'source_time_pack_ids[]',
                    });"
                  >
                  <span style="font-weight: 500; font-style: italic;">Materi:</span>
                </label>
              </p>
              <ul style="list-style: none; padding: 0px; margin: 0px;">
                <?php foreach ($time_packs as $time_pack): ?>
                <li style="padding: .75rem 0px;">
                  <label style="display: block; white-space: nowrap;">
                    <input
                      type="checkbox"
                      name="source_time_pack_ids[]"
                      value="<?= $time_pack["id"] ?>"
                      data-checked="no"
                      onchange="this.dataset.checked = this.checked ? 'yes' : 'no'"
                    >
                    <span><?= $time_pack["name"] ?></span>
                  </label>
                </li>
                <?php endforeach ?>
              </ul>
            </div>
            <div style="width: 33.333%; padding: .75rem; list-style: none; background-color: #efefef; border-top-right-radius: .5rem; border-bottom-right-radius: .5rem;">
              <p style="margin-top: 0px; margin-bottom: .5rem; white-space: nowrap;">
                <label>
                  <input
                    type="checkbox"
                    name="source_timing_basis_all"
                    value="yes"
                    onchange="ev.publish('schedule:toogle_select_all', {
                      condition: this.checked,
                      name: 'source_timing_basiss[]',
                    });"
                  >
                  <span style="font-weight: 500; font-style: italic;">Basis Waktu:</span>
                </label>
              </p>
              <ul style="list-style: none; padding: 0px; margin: 0px;">
                <?php foreach ($time_packs as $time_pack): ?>
                <li style="padding: .75rem 0px;">
                  <label style="display: block; white-space: nowrap;">
                    <input
                      type="checkbox"
                      name="source_timing_basiss[]"
                      value="<?= $time_pack["id"] ?>"
                      data-checked="no"
                      onchange="this.dataset.checked = this.checked ? 'yes' : 'no'"
                    >
                    <span><?= $time_pack["name"] ?></span>
                  </label>
                </li>
                <?php endforeach ?>
              </ul>
            </div>
            <div style="width: 33.333%; padding: .75rem; list-style: none; background-color: #ffffff; border-top-right-radius: .5rem; border-bottom-right-radius: .5rem;">
              <p style="margin-top: 0px; margin-bottom: .5rem;">
                <label style="white-space: nowrap;">
                  <input
                    type="checkbox"
                    name="target_day_all"
                    value="yes"
                    onchange="ev.publish('schedule:toogle_select_all', {
                      condition: this.checked,
                      name: 'target_day_ids[]',
                    });"
                  >
                  <span style="font-weight: 500; font-style: italic;">Ke Hari:</span>
                </label>
              </p>
              <ul style="list-style: none; padding: 0px; margin: 0px;">
                <?php foreach ($days as $day): ?>
                <li style="padding: .75rem 0px;">
                  <label style="display: block">
                    <input
                      type="checkbox"
                      name="target_day_ids[]"
                      value="<?= $day["id"] ?>"
                      <?= $day["id"] === $active_day["id"] ? "checked disabled data-disabled='yes'" : "" ?>
                      data-checked="no"
                      onchange="this.dataset.checked = this.checked ? 'yes' : 'no'"
                    >
                    <span><?= $day["label"] ?></span>
                  </label>
                </li>
                <?php endforeach ?>
              </ul>
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: schedule_copy_dialog })"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-content_copy-24px.svg" ?>
              </span>
              <span class="text">Terapkan</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /SCHEDULE_COPY_DIALOG -->
  </div>

  <?php export_to_js("scheduleTime", $schedule_time); ?>

  <?php
echo "<script>";
require_once __DIR__ . "/schedule.js";
echo "</script>";
  ?>
</body>

</html>
