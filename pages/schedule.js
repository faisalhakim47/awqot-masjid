var urlQueryString = location.search
  .slice(1)
  .split('&')
  .map(function (param) {
    return param.split('=');
  })
  .reduce(function (params, param) {
    params[param[0]] = decodeURIComponent(param[1]);
    return params;
  }, {});
var dayId = urlQueryString.day_id;
var scheduleId = urlQueryString.schedule_id;
var timePackId = urlQueryString.time_pack_id;

document.addEventListener('DOMContentLoaded', function () {
  document.querySelector('#materi_selector [name="action"]').value = 'add_schedule_time_audios';
  var input = document.createElement('input');
  input.type = 'hidden';
  input.name = 'schedule_time_id';
  input.value = scheduleTime.id;
  document.querySelector('#materi_selector form').appendChild(input);
  Array.prototype.forEach.call(document.querySelectorAll('.page-header-tab-item.activated'), function (element) {
    element.scrollIntoView({ behavior: "auto" });
  });
});

function select(selector) {
  var elements = document.querySelectorAll(selector);
  return Array.prototype.slice.call(elements);
}

function openAudioMateriPlayer() {
  var audio_hash = select('.audio-item.selected')
    .map(function (li) {
      return li.dataset.hash;
    })[0];
  ev.publish('materi_player:open', { audio_hash: audio_hash });
}

function toggleDaySelect(li) {
  li.classList.toggle('selected');
  var input = li.querySelector('input.target_day_ids');
  input.value = li.classList.contains('selected')
    ? input.dataset.value
    : null;
}

window.addEventListener('click', function (event) {
  if (event.target === document.children[0]) {
    select('.audio-item').forEach(function (audioItem) {
      ev.publish('schedule:audio_unselect', { item: audioItem });
    });
  }
});

ev.subscribe('schedule:open_audio_delete_modal', function open_audio_delete_modal() {
  ev.publish('modal:open', { dialog: audios_delete });
  var deletedAudios = select('.audio-item.selected')
    .map(function (li) {
      return li.dataset.hash;
    });
  select('.audio-item')
    .map(function (li) {
      return li.dataset.hash;
    })
    .filter((audio_hash) => {
      return deletedAudios.indexOf(audio_hash) === -1;
    })
    .map(function (audio_hash) {
      var input = document.createElement('input');
      input.type = 'hidden';
      input.name = 'audio_hashes[]';
      input.value = audio_hash;
      return input;
    })
    .reduce(function (container, input) {
      container.appendChild(input);
      return container;
    }, document.querySelector('#audios_delete .modal-content'));
});

ev.subscribe('schedule:close_audio_delete_modal', function close_audio_delete_modal() {
  ev.publish('modal:close', { dialog: audios_delete });
  select('#audios_delete input[name="audio_hashes[]"]')
    .reduce(function (container, input) {
      container.removeChild(input);
      return container;
    }, document.querySelector('#audios_delete .modal-content'));
});

ev.subscribe('schedule:set_audio_position', function set_audio_position(data) {
  var direction = data.direction;
  select('.button.position-control').forEach(function (control) {
    control.disabled = true;
  });
  var request = new XMLHttpRequest();
  var data = new FormData();
  var audioInput = select('.schedule-controls [name="selected_audios[]"]')[0];
  var audioHash = audioInput.value;
  var audioIndex = parseInt(audioInput.dataset.index, 10);
  data.append('schedule_time_id', scheduleTime.id);
  data.append('audio_hash', audioHash);
  data.append('audio_index', audioIndex);
  data.append('direction', direction);
  request.open('POST', '/api/schedule_time_audio_position_set.php', true);
  request.addEventListener('readystatechange', () => {
    if (request.readyState !== XMLHttpRequest.DONE) return;
    if (request.status !== 200) return alert(`MASALAH SERVER: ${request.responseText}`);
    var audioItemEl = document.querySelectorAll('.audio-item').item(audioIndex);
    if (direction === 'up' && audioItemEl.previousElementSibling) {
      audioItemEl.previousElementSibling.insertAdjacentElement('beforebegin', audioItemEl);
      audioInput.dataset.index = (audioIndex - 1).toString();
    } else if (direction === 'down' && audioItemEl.nextElementSibling) {
      audioItemEl.nextElementSibling.insertAdjacentElement('afterend', audioItemEl);
      audioInput.dataset.index = (audioIndex + 1).toString();
    }
    select('.button.position-control').forEach(function (control) {
      control.disabled = false;
    });
  });
  request.addEventListener('error', () => {
    alert(`MASALAH KONEKSI`);
  });
  request.send(data);
});

ev.subscribe('schedule:audio_select', function audio_select(data) {
  if (data.item.classList.contains('selected')) return;
  data.item.classList.add('selected');
  var input = document.createElement('input');
  input.type = 'hidden';
  input.name = 'selected_audios[]';
  input.value = data.item.dataset.hash;
  input.dataset.index = select('.audio-item').indexOf(data.item);
  document.querySelector('.schedule-controls').appendChild(input);
  ev.publish('schedule:audio_toggle_select_change');
});

ev.subscribe('schedule:audio_unselect', function audio_unselect(data) {
  if (!data.item.classList.contains('selected')) return;
  data.item.classList.remove('selected');
  var input = document.querySelector('.schedule-controls [value="' + data.item.dataset.hash + '"]');
  input.parentNode.removeChild(input);
  ev.publish('schedule:audio_toggle_select_change');
});

ev.subscribe('schedule:audio_toggle_select', function audio_toggle_select(data) {
  if (data.item.classList.contains('selected')) {
    ev.publish('schedule:audio_unselect', data);
  } else {
    select('.audio-item.selected').forEach(function (element) {
      element.classList.remove('selected');
    });
    ev.publish('schedule:audio_select', data);
  }
});

ev.subscribe('schedule:audio_toggle_select_change', function audio_toggle_select_change() {
  var selectedLength = document.querySelectorAll('.audio-item.selected').length;
  if (selectedLength === 0) {
    document.body.classList.remove('audio-selected');
  } else {
    document.body.classList.add('audio-selected');
    select('.button.position-control').forEach(function (control) {
      control.disabled = selectedLength > 1;
    });
  }
});

ev.subscribe('schedule:toogle_select_all', function toogle_select_all(options) {
  select('input[name="' + options.name + '"]').forEach(function (input) {
    if (input.dataset.disabled === 'yes') {
      return;
    }
    input.checked = options.condition;
    input.disabled = options.condition;
    if (!input.checked) {
      input.checked = input.dataset.checked === 'yes';
    }
  });
});

function timing_basis_select_change() {
  if (timing_basis_input.value === 'kemenag') {
    timing_manual_time_field.style.display = 'none';
    timing_manual_time_input.disabled = true;
    // timing_istiwa_time_field.style.display = 'none';
    // timing_istiwa_time_input.disabled = true;
  } else if (timing_basis_input.value === 'manual') {
    timing_manual_time_field.style.display = 'block';
    timing_manual_time_input.disabled = false;
    // timing_istiwa_time_field.style.display = 'none';
    // timing_istiwa_time_input.disabled = true;
  } else if (timing_basis_input.value === 'istiwa') {
    timing_manual_time_field.style.display = 'none';
    timing_manual_time_input.disabled = true;
    // timing_istiwa_time_field.style.display = 'block';
    // timing_istiwa_time_input.disabled = false;
  }
  if (timing_basis_input.value === 'kemenag') {
    timing_basis_info.style.display = 'block';
  } else {
    timing_basis_info.style.display = 'none';
  }
}

ev.subscribe('schedule:timing_basis_select_change', timing_basis_select_change);
timing_basis_select_change();
