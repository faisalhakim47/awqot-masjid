<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/child_process.php";
require_once __DIR__ . "/../api/tools/database.php";
require_once __DIR__ . "/../api/tools/timezone.php";

// ----- CONTROLLER -----

if (isset($_POST["action"])) switch ($_POST["action"]) {
  case "timezone_set":
  set_timezone($_POST["timezone"]);
  $new_city_id = (int) $_POST["city_id"];
  set_configuration("city_id", $new_city_id);
  $time_packs = execute_sql("
    SELECT id, name
    FROM time_packs
    WHERE is_disabled = 0
    ORDER BY id ASC
  ")->fetchAll();
  foreach ($time_packs as $time_pack) {
    $ihtiyat_name = "ihtiyat_{$time_pack["name"]}";
    set_configuration($ihtiyat_name, $_POST[$ihtiyat_name]);
  }
  async_exec("php-cgi " . realpath(__DIR__ . "/../api/commands/ensure_current_times.php"));
  break;
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  require_once __DIR__ . "/../api/commands/scheduling_compute.php";
  header("Location: {$_SERVER["REQUEST_URI"]}");
  exit();
}

// ----- MODEL -----

$available_timezones = get_timezones();
$current_timezone = get_timezone();

$provinces = execute_sql("
  SELECT
    provinces.id AS id,
    provinces.name AS name
  FROM provinces
")->fetchAll();

$city_id = (int) get_configuration("city_id");
$ihtiyat = (int) get_configuration("ihtiyat");

$province_id = (int) execute_sql("
  SELECT cities.province_id AS province_id
  FROM cities
  WHERE id = :city_id
", [
  ":city_id" => [$city_id, PDO::PARAM_INT],
])->fetch()["province_id"];

$cities = execute_sql("
  SELECT
    cities.id AS id,
    cities.name AS name
  FROM cities
  WHERE cities.province_id = :province_id
", [
  ":province_id" => [$province_id, PDO::PARAM_INT],
])->fetchAll();

$ihtiyats = array_map(function ($time_pack) {
  $ihtiyat_name = "ihtiyat_{$time_pack["name"]}";
  return [
    "name" => $ihtiyat_name,
    "time_pack" => $time_pack,
    "value" => get_configuration($ihtiyat_name),
  ];
}, execute_sql("
  SELECT id, name
  FROM time_packs
  WHERE is_disabled = 0
  ORDER BY id ASC
")->fetchAll());

// ----- VIEW -----

require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/box.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_group.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/notification.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/sidebar.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Zona Waktu - Awqot</title>
  <?php publish("head"); ?>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 3.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <span class="icon">
              <?php include __DIR__ . "/../static/icons/round-menu-24px.svg"; ?>
            </span>
          </button>
          <h1 class="page-title">Waktu dan Wilayah</h1>
        </div>
      </div>
    </header>

    <main class="page-content padded">
      <section class="box">
        <form class="box-content" method="post">
          <input type="hidden" name="action" value="timezone_set">
          <div class="field">
            <div class="field-label">
              <label for="timezone_input">Zona Waktu</label>
            </div>
            <div class="field-input">
              <select id="timezone_input" name="timezone">
                <?php foreach ($available_timezones as $option): ?>
                <option
                  value="<?= $option ?>"
                  <?= $option === $current_timezone ? "selected" : "" ?>
                ><?= $option ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="province_id_input">Provinsi</label>
            </div>
            <div class="field-input">
              <select id="province_id_input" name="province_id" onchange="reloadCity(this.value)">
                <?php foreach ($provinces as $province): ?>
                <option
                  value="<?= $province["id"] ?>"
                  <?= $province["id"] === $province_id ? "selected" : "" ?>
                ><?= $province["name"] ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="city_id_input">Kota</label>
            </div>
            <div class="field-input">
              <select id="city_id_input" name="city_id">
                <?php foreach ($cities as $city): ?>
                <option
                  value="<?= $city["id"] ?>"
                  <?= $city["id"] === $city_id ? "selected" : "" ?>
                ><?= $city["name"] ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="ihtiyat_Subuh_input">Ihtiyat (detik)</label>
            </div>
            <?php foreach ($ihtiyats as $ihtiyat): ?>
            <div class="field-input">
              <label for="<?= $ihtiyat["name"] ?>_input" class="field-input-addons"><?= $ihtiyat["time_pack"]["name"] ?></label>
              <input id="<?= $ihtiyat["name"] ?>_input" type="number" name="<?= $ihtiyat["name"] ?>" value="<?= $ihtiyat["value"] ?>">
              <div class="field-input-addons">detik</div>
            </div>
            <?php endforeach; ?>
            <p class="field-info">Jadwal waktu sholat dapat disesuaikan dengan mengisi Ihtiyat dalam detik. Isi dengan bilangan positif untuk menunda waktu sholat dan isi dengan bilangan negatif untuk mempercepat waktu sholat. Contoh: apabila Ihtiyat diisi dengan 60, maka jadwal waktu sholat akan mundur 60 detik atau satu menit.</p>
          </div>
          <div class="button-group align-right">
            <button type="submit" class="button primary solid">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Simpan</span>
            </button>
          </div>
        </form>
      </section>
    </main>
  </div>

  <script>
    document.addEventListener("DOMContentLoaded", function () {
      var cityIdInput = document.getElementById("city_id_input");
      window.reloadCity = function reloadCity(provinceId) {
        var request = new XMLHttpRequest();
        var data = new FormData();
        request.open("GET", "/api/city-list.php?province_id=" + provinceId, true);
        request.addEventListener("readystatechange", function () {
          if (request.readyState === request.DONE && request.status === 200) {
            cityIdInput.innerHTML = JSON.parse(request.responseText)
              .reduce(function (options, city) {
                return options + '<option value="' + city.id + '">' + city.name + '</option>';
              }, '<option>-</option>');
          }
        });
        request.send(data);
      }
    });
  </script>
</body>

</html>
