$fn = PI * 20;

tolerance = .2;

outlet = 56.5;
outlet_inner = 41;
outlet_wall = 1.5;
outlet_depth = 18;
outlet_screw = ((outlet - outlet_inner) / 2) - outlet_wall;
outlet_screw_depth = 1.5;

box_shell = 3;
box_x = 210;
box_y = 145 + outlet_depth;
box_z = outlet;

rpi_x = 56;
rpi_y = 85;
rpi_z = 1.6;
rpi_usb_x = 15.5;
rpi_usb_z = 16.25;
rpi_lan_x = 16.25;
rpi_lan_z = 14;
rpi_pos_z = 7.5;
rpi_lan_relpos = 10.25;
rpi_usb1_relpos = 29;
rpi_usb2_relpos = 47;
rpi_jack_relpos = 53.5;
rpi_hdmi_relpos = 32;
rpi_musb_relpos = 10.6;
rpi_screw_outer = 6;
rpi_screw_inner = 2.75;
rpi_screw_relpos_side = 3.5;
rpi_screw_x_between = 49;
rpi_screw_y_between = 58;

power_screw_d = 4;
power_screw_relpos_y_side = 1.5;
power_screw_relpos_x_side = 4;
power_screw_relpos_y_between = 0;

relay_x = 76;
relay_y = 56;
relay_screw_d = 3;
relay_screw_x_between = 69.6;
relay_screw_y_between = 49.4;
relay_screw_relpos_side = (relay_x - relay_screw_x_between) / 2;

terminal_width = 7.6;
terminal_space = 3.4;
terminal_between = terminal_width + terminal_space;

module HDMI(length=10) {
	translate([
		0,
		0,
		-length / 2
	]) {
		 linear_extrude(
			height=length,
			center=true
		) {
			polygon(
				points=[
					[8, 3.25],
					[-8, 3.25],
					[-8, -1],
					[-6.25, -3.25],
					[6.25, -3.25],
					[8, -1],
				]
			);
		}
	}
}

module micro_usb(length=10) {
	translate([
		0,
		0,
		-length / 2
	]) {
		linear_extrude(
			height=length,
			center=true
		) {
			polygon(
				points=[
					[4.25, 2],
					[-4.25, 2],
					[-4.75, 1.5],
					[-4.75, -0.5],
					[-3.25, -2],
					[3.25, -2],
					[4.75, -0.5],
					[4.75, 1.5]
				]
			);
		}
	}
}


// ----- ALAS -----
translate([
  0,
  0,
  box_shell / 2
]) {
	difference() {
		cube ([
			box_x,
			box_y,
			box_shell
		], center=true);  
    
    // ----- Lubang DS3231 -----
		translate([
			-60,
			-14,
			0
		]) {
			cylinder(
		    r=11.5,
			  h=box_shell + 1,
			  center=true
			);
			translate([
				11.25,
				0,
				0
			]) {
				cube([
					5,
					8.5,
					box_shell + 1
				], center=true);
			}
		}
  }
  
  // ---- Raspberry Pi Skrup ----
	translate([
		(box_x / 2) - box_shell - rpi_screw_relpos_side,
		(box_y / 2) - box_shell - rpi_y + rpi_screw_relpos_side,
		(rpi_pos_z / 2) + (box_shell / 2) - 1
	]) {
		difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		translate ([
			-rpi_screw_x_between,
			0,
			0
		]) {
			difference() {
				cylinder(
					r=rpi_screw_outer / 2,
					h=rpi_pos_z + 2,
					center=true
				);
				cylinder(
					r=rpi_screw_inner / 2,
					h=rpi_pos_z + 3,
					center=true
				);
			}
		}
		translate ([
			-rpi_screw_x_between,
			rpi_screw_y_between,
			0
		]) {
			difference() {
				cylinder(
					r=rpi_screw_outer / 2,
					h=rpi_pos_z + 2,
					center=true
				);
				cylinder(
					r=rpi_screw_inner / 2,
					h=rpi_pos_z + 3,
					center=true
				);
			}
		}
		translate ([
			0,
			rpi_screw_y_between,
			0
		]) {
			difference() {
				cylinder(
					r=rpi_screw_outer / 2,
					h=rpi_pos_z + 2,
					center=true
				);
				cylinder(
					r=rpi_screw_inner / 2,
					h=rpi_pos_z + 3,
					center=true
				);
			}
		}
	} // ---- /Raspberry Pi Skrup ----

	// ---- Relay Skrup ----
	translate([
		-(box_x / 2) + box_shell + relay_screw_relpos_side,
		(box_y / 2) - box_shell - relay_y + relay_screw_relpos_side,
		(rpi_pos_z / 2) + (box_shell / 2) - 1
	]) {
		translate([
			0,
			relay_screw_d / 2 + 1.5,
			0
		]) {
			difference() {
				cylinder(
					r=(relay_screw_d + 3) / 2,
					h=rpi_pos_z + 2,
					center=true
				);
				cylinder(
					r=relay_screw_d / 2,
					h=rpi_pos_z + 3,
					center=true
				);
			}
		}
		translate ([
			relay_screw_x_between,
			relay_screw_d / 2 + 1.5,
			0
		]) {
			difference() {
				cylinder(
					r=(relay_screw_d + 3) / 2,
					h=rpi_pos_z + 2,
					center=true
				);
				cylinder(
					r=relay_screw_d / 2,
					h=rpi_pos_z + 3,
					center=true
				);
			}
		}
		translate ([
			relay_screw_x_between,
			relay_screw_y_between,
			0
		]) {
			difference() {
				cylinder(
					r=(relay_screw_d + 3) / 2,
					h=rpi_pos_z + 2,
					center=true
				);
				cylinder(
					r=relay_screw_d / 2,
					h=rpi_pos_z + 3,
					center=true
				);
			}
		}
		translate ([
			0,
			relay_screw_y_between,
			0
		]) {
			difference() {
				cylinder(
					r=(relay_screw_d + 3) / 2,
					h=rpi_pos_z + 2,
					center=true
				);
				cylinder(
					r=relay_screw_d / 2,
					h=rpi_pos_z + 3,
					center=true
				);
			}
		}
	} // ---- /Relay Skrup ----

	// ---- Power Skrup ----
	translate([
		box_x / 2 - box_shell - (rpi_screw_outer / 2) - power_screw_relpos_x_side - rpi_x - 2,
		box_y / 2 - box_shell - (rpi_screw_outer / 2) - power_screw_relpos_y_side,
		(rpi_pos_z / 2) + (box_shell / 2) - 1
	]) {
		translate ([
			0,
			-80,
			0
		]) difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		translate ([
			0,
			0,
			0
		]) difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		translate ([
			-47,
			0 ,
			0
		]) difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		translate ([
			-47,
			-80,
			0
		]) difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
	} // ---- /Power Skrup ----

	// ---- DS3231 Skrup ----
	translate([
		-60,
		-14,
		4 / 2
	]) {
		translate([
			-14,
			-9,
			0
		]) {
			difference() {
				cylinder(
					r=3,
					h=box_shell + 4,
					center=true
				);
				cylinder(
					r=1,
					h=box_shell + 6,
					center=true
				);
			}
		}
		translate([
			-14,
			9,
			0
		]) {
			difference() {
				cylinder(
					r=3,
					h=box_shell + 4,
					center=true
				);
				cylinder(
					r=1,
					h=box_shell + 6,
					center=true
				);
			}
		}
		translate([
			12,
			9,
			0
		]) {
			difference() {
				cylinder(
					r=3,
					h=box_shell + 4,
					center=true
				);
				cylinder(
					r=1,
					h=box_shell + 6,
					center=true
				);
			}
		}
		translate([
			12,
			-9,
			0
		]) {
			difference() {
				cylinder(
					r=3,
					h=box_shell + 4,
					center=true
				);
				cylinder(
					r=1,
					h=box_shell + 6,
					center=true
				);
			}
		}
	} // ---- /DS3231 Skrup ----
}

// ----- KANAN -----
translate([
  box_x / 2 - (box_shell / 2),
  0,
  box_z / 2 + box_shell
]) {
	difference() {
		cube ([
			box_shell,
			box_y,
			box_z
		], center=true);
    
    translate([
			0,
			(box_y / 2) - box_shell,
			-((box_z - box_shell) / 2) + box_shell + rpi_pos_z + rpi_z
		]) {
			// ---- Jack 3.5 ----
			translate([
				0,
				-rpi_y + rpi_jack_relpos,
				((3.5 * 2) / 2) - (((3.5 * 2) - 5.5) / 2)
			]) {
				rotate([
					90,
					0,
					90
				]) {
					cylinder(
						r=3.5 + tolerance,
						h=5,
						center=true
					);
				}
			}
			// ---- HDMI ----
			translate([
				5,
				-rpi_y + rpi_hdmi_relpos - 0.5,
				(7 / 2) + 0.5
			]) {
				rotate([
					90,
					0,
					90
				]) {
					HDMI(10);
				}
			}
			// ---- micro usb ----
			translate([
				5,
				-rpi_y + rpi_musb_relpos - 0.5,
				(4 / 2) - 0.5
			]) {
				rotate([
					90,
					0,
					90
				]) {
					micro_usb(10);
				}
			}
		}
  }
}

// ----- KIRI -----
translate([
  -box_x / 2 + (box_shell / 2),
  0,
  box_z / 2 + box_shell
]) {
	difference() {
		cube ([
			box_shell,
			box_y,
			box_z
		], center=true);
    
    translate([
      0,
      0,
      0
    ]) {
      translate([
      0,
      0,
      0
    ]) {
        cylinder(
          r=terminal_width,
          
        );
      }
    }
  }
}

// ----- BELAKANG OUTLET 1 -----
translate([
  ((box_x - (outlet * 1)) / 2) - box_shell,
  -box_y / 2 + (box_shell / 2) + outlet_depth,
  box_z / 2 + box_shell
]) {
	difference() {
		cube ([
			outlet,
			box_shell,
			box_z
		], center=true);
    
    cube ([
      outlet_inner,
      box_shell + 1,
      box_z + 1
    ], center=true);
  }

  translate([
    (outlet / 2) - ((outlet - outlet_inner) / 2) + (outlet_screw / 2),
    -(box_shell / 2) - (outlet_screw_depth / 2),
    0
  ]) {
    difference() {
      cube ([
        outlet_screw,
        outlet_screw_depth,
        box_z / 4
      ], center=true);
      
      rotate([
        0,
				90,
				90
      ]) cylinder(
        r=1,
        h=10,
        center=true
      );
      
      translate ([
        0,
        -(box_z / 16) + (outlet_screw_depth / 2),
        -(box_z / 8)
      ]) rotate([
        90,
				0,
				90
      ]) cylinder(
        r=box_z / 16,
        h=20,
        center=true
      );
    } 
  }

  translate([
    -(outlet / 2) + ((outlet - outlet_inner) / 2) - (outlet_screw / 2),
    -(box_shell / 2) - (outlet_screw_depth / 2),
    0
  ]) {
    difference() {
      cube ([
        outlet_screw,
        outlet_screw_depth,
        box_z / 4
      ], center=true);
      
      rotate([
        0,
				90,
				90
      ]) cylinder(
        r=1,
        h=10,
        center=true
      );
      
      translate ([
        0,
        -(box_z / 16) + (outlet_screw_depth / 2),
        -(box_z / 8)
      ]) rotate([
        90,
				0,
				90
      ]) cylinder(
        r=box_z / 16,
        h=20,
        center=true
      );
    }
  }
}

// ----- BELAKANG OUTLET 2 -----
translate([
  ((box_x - (outlet * 3)) / 2) - box_shell,
  -box_y / 2 + (box_shell / 2) + outlet_depth,
  box_z / 2 + box_shell
]) {
	difference() {
		cube ([
			outlet,
			box_shell,
			box_z
		], center=true);
    
    cube ([
      outlet_inner,
      box_shell + 1,
      box_z + 1
    ], center=true);    
  }
  
  translate([
    (outlet / 2) - ((outlet - outlet_inner) / 2) + (outlet_screw / 2),
    -(box_shell / 2) - (outlet_screw_depth / 2),
    0
  ]) {
    difference() {
      cube ([
        outlet_screw,
        outlet_screw_depth,
        box_z / 4
      ], center=true);
      
      rotate([
        0,
				90,
				90
      ]) cylinder(
        r=1,
        h=10,
        center=true
      );
      
      translate ([
        0,
        -(box_z / 16) + (outlet_screw_depth / 2),
        -(box_z / 8)
      ]) rotate([
        90,
				0,
				90
      ]) cylinder(
        r=box_z / 16,
        h=20,
        center=true
      );
    } 
  }

  translate([
    -(outlet / 2) + ((outlet - outlet_inner) / 2) - (outlet_screw / 2),
    -(box_shell / 2) - (outlet_screw_depth / 2),
    0
  ]) {
    difference() {
      cube ([
        outlet_screw,
        outlet_screw_depth,
        box_z / 4
      ], center=true);
      
      rotate([
        0,
				90,
				90
      ]) cylinder(
        r=1,
        h=10,
        center=true
      );
      
      translate ([
        0,
        -(box_z / 16) + (outlet_screw_depth / 2),
        -(box_z / 8)
      ]) rotate([
        90,
				0,
				90
      ]) cylinder(
        r=box_z / 16,
        h=20,
        center=true
      );
    }
  }
}

// ----- BELAKANG OUTLET 3 -----
translate([
  ((box_x - (outlet * 5)) / 2) - box_shell,
  -box_y / 2 + (box_shell / 2) + outlet_depth,
  box_z / 2 + box_shell
]) {
	difference() {
		cube ([
			outlet,
			box_shell,
			box_z
		], center=true);
    
    cube ([
      outlet_inner,
      box_shell + 1,
      box_z + 1
    ], center=true);    
  }
  
  translate([
    (outlet / 2) - ((outlet - outlet_inner) / 2) + (outlet_screw / 2),
    -(box_shell / 2) - (outlet_screw_depth / 2),
    0
  ]) {
    difference() {
      cube ([
        outlet_screw,
        outlet_screw_depth,
        box_z / 4
      ], center=true);
      
      rotate([
        0,
				90,
				90
      ]) cylinder(
        r=1,
        h=10,
        center=true
      );
      
      translate ([
        0,
        -(box_z / 16) + (outlet_screw_depth / 2),
        -(box_z / 8)
      ]) rotate([
        90,
				0,
				90
      ]) cylinder(
        r=box_z / 16,
        h=20,
        center=true
      );
    } 
  }

  translate([
    -(outlet / 2) + ((outlet - outlet_inner) / 2) - (outlet_screw / 2),
    -(box_shell / 2) - (outlet_screw_depth / 2),
    0
  ]) {
    difference() {
      cube ([
        outlet_screw,
        outlet_screw_depth,
        box_z / 4
      ], center=true);
      
      rotate([
        0,
				90,
				90
      ]) cylinder(
        r=1,
        h=10,
        center=true
      );
      
      translate ([
        0,
        -(box_z / 16) + (outlet_screw_depth / 2),
        -(box_z / 8)
      ]) rotate([
        90,
				0,
				90
      ]) cylinder(
        r=box_z / 16,
        h=20,
        center=true
      );
    }
  }
}

// ----- BELAKANG 2 -----
translate([
  -box_x / 2 + (box_shell / 2) + box_x - (outlet * 3) - (box_shell * 2),
  -box_y / 2 + (box_shell / 2) + (outlet_depth / 2),
  box_z / 2 + box_shell
]) {
	difference() {
		cube ([
			box_shell,
			outlet_depth + box_shell,
			box_z
		], center=true);  
  }
}

// ----- BELAKANG 3 -----
translate([
  -box_x / 2 + ((box_x - (outlet * 3)) / 2) - (box_shell / 2),
  -box_y / 2 + (box_shell / 2),
  box_z / 2 + box_shell
]) {
	difference() {
		cube ([
			box_x - (outlet * 3) - box_shell,
			box_shell,
			box_z
		], center=true);  
  }
}

// ----- TUTUP -----
*union() {

// ----- DEPAN -----
translate([
  0,
  box_y / 2 - (box_shell / 2) + 10,
  box_z / 2 + box_shell + 10
]) {
	difference() {
		cube ([
			box_x - (box_shell * 2) - tolerance,
			box_shell,
			box_z
		], center=true);
    
    translate([
			(box_x / 2) - box_shell,
			0,
			-((box_z - box_shell) / 2) + box_shell + rpi_pos_z + rpi_z
		]) {
			// ----- LAN -----
			translate([
				-rpi_lan_relpos,
				0,
				rpi_lan_z / 2
			]) {
				cube([
					rpi_lan_x,
					box_shell + 1,
					rpi_lan_z
				], center=true);
			}
			// ---- USB 1 ----
			translate([
				-rpi_usb1_relpos,
				0,
				rpi_usb_z / 2
			]) {
				cube([
					rpi_usb_x,
					box_shell + 1,
					rpi_usb_z
				], center=true);
			}
			// ---- USB 2 ----
			translate([
				-rpi_usb2_relpos,
				0,
				rpi_usb_z / 2
			]) {
				cube([
					rpi_usb_x,
					box_shell + 1,
					rpi_usb_z
				], center=true);
			}
		}
  }
}

// ----- ATAS -----
translate([
  0,
  10,
  box_z + box_shell + (box_shell / 2) + 10
]) {
	difference() {
		cube ([
			box_x,
			box_y,
			box_shell
		], center=true);  
  }
}

}
