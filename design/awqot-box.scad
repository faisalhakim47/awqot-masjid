$fn = PI * 20;

outlet = 57.5;
outlet_screw = 7.5;
outlet_screw_holder = 5.25;
box_x = 210;
box_y = 145;
box_z = outlet;
box_shell = 4;

rpi_x = 56;
rpi_y = 85;
rpi_z = 1.6;
rpi_usb_x = 15.5;
rpi_usb_z = 16.25;
rpi_lan_x = 16.25;
rpi_lan_z = 14;
rpi_pos_z = 7.5;
rpi_lan_relpos = 10.25;
rpi_usb1_relpos = 29;
rpi_usb2_relpos = 47;
rpi_jack_relpos = 53.5;
rpi_hdmi_relpos = 32;
rpi_musb_relpos = 10.6;
rpi_screw_outer = 6;
rpi_screw_inner = 2.75;
rpi_screw_relpos_side = 3.5;
rpi_screw_x_between = 49;
rpi_screw_y_between = 58;

power_screw_d = 4;
power_screw_relpos_y_side = 1.5;
power_screw_relpos_x_side = 4;
power_screw_relpos_y_between = 0;

relay_x = 76;
relay_y = 56;
relay_screw_d = 3;
relay_screw_x_between = 69.6;
relay_screw_y_between = 49.4;
relay_screw_relpos_side = (relay_x - relay_screw_x_between) / 2;

module HDMI(length=10) {
	translate([
		0,
		0,
		-length / 2
	]) linear_extrude(
		height=length,
		center=true
	) polygon(
		points=[
			[8, 3.25],
			[-8, 3.25],
			[-8, -1],
			[-6.25, -3.25],
			[6.25, -3.25],
			[8, -1],
		]
	);
}

module micro_usb(length=10) {
	translate([
		0,
		0,
		-length / 2
	]) linear_extrude(
		height=length,
		center=true
	) polygon(
		points=[
			[4.25, 2],
			[-4.25, 2],
			[-4.75, 1.5],
			[-4.75, -0.5],
			[-3.25, -2],
			[3.25, -2],
			[4.75, -0.5],
			[4.75, 1.5]
		]
	);
}

// ----- BOTTOM -----
translate([
	0,
	0,
	box_shell / 2
]) {
	difference() {
		cube ([
			box_x,
			box_y,
			box_shell
		], center=true);
		translate([
			-(box_x / 2) + box_shell + 6,
			-(box_y / 2) + box_shell + 9.5,
			2
		]) cube([
			21,
			29,
			4
		], center=true);
		//	---- DS3231 Hole ----
		translate([
			-60,
			-14,
			0
		]) {
			cylinder(
			 r=11.5,
			 h=box_shell + 1,
			 center=true
			);
			translate([
				11.25,
				0,
				0
			]) cube([
				5,
				8.5,
				box_shell + 1
			], center=true);
		}
		// ---- body screws ----
		translate ([
			box_x / 2 - box_shell - 3,
			-25,
			-2
		]) cylinder(
			r=(relay_screw_d + 3) / 2 + .5,
			h=box_shell + 1.01,
			center=true
		);
		translate ([
			-(box_x / 2) + box_shell + 3,
			2.5,
			-2
		]) cylinder(
			r=(relay_screw_d + 3) / 2 + .5,
			h=box_shell + 1.01,
			center=true
		);
	}
	cylinder(
		r=1.5
	);
	// ---- Raspberry Pi Screws ----
	translate([
		(box_x / 2) - box_shell - rpi_screw_relpos_side,
		(box_y / 2) - box_shell - rpi_y + rpi_screw_relpos_side,
		(rpi_pos_z / 2) + (box_shell / 2) - 1
	]) {
		difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		translate ([
			-rpi_screw_x_between,
			0,
			0
		]) difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		translate ([
			-rpi_screw_x_between,
			rpi_screw_y_between,
			0
		]) difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		translate ([
			0,
			rpi_screw_y_between,
			0
		]) difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
	}
	// ---- Relay Screws ----
	translate([
		-(box_x / 2) + box_shell + relay_screw_relpos_side,
		(box_y / 2) - box_shell - relay_y + relay_screw_relpos_side,
		(rpi_pos_z / 2) + (box_shell / 2) - 1
	]) {
		translate([
			0,
			relay_screw_d / 2 + 1.5,
			0
		]) difference() {
			cylinder(
				r=(relay_screw_d + 3) / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=relay_screw_d / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		
		translate ([
			relay_screw_x_between,
			relay_screw_d / 2 + 1.5,
			0
		]) difference() {
			cylinder(
				r=(relay_screw_d + 3) / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=relay_screw_d / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		
		translate ([
			relay_screw_x_between,
			relay_screw_y_between,
			0
		]) difference() {
			cylinder(
				r=(relay_screw_d + 3) / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=relay_screw_d / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		translate ([
			0,
			relay_screw_y_between,
			0
		]) difference() {
			cylinder(
				r=(relay_screw_d + 3) / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=relay_screw_d / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
	}
	// ---- Power Screws ----
	translate([
		box_x / 2 - box_shell - (rpi_screw_outer / 2) - power_screw_relpos_x_side - rpi_x - 2,
		box_y / 2 - box_shell - (rpi_screw_outer / 2) - power_screw_relpos_y_side,
		(rpi_pos_z / 2) + (box_shell / 2) - 1
	]) {
		translate ([
			0,
			-80,
			0
		]) difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		translate ([
			0,
			0,
			0
		]) difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		translate ([
			-47,
			0 ,
			0
		]) difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
		translate ([
			-47,
			-80,
			0
		]) difference() {
			cylinder(
				r=rpi_screw_outer / 2,
				h=rpi_pos_z + 2,
				center=true
			);
			cylinder(
				r=rpi_screw_inner / 2,
				h=rpi_pos_z + 3,
				center=true
			);
		}
	}
	// ---- body screws ----
	translate ([
		box_x / 2 - box_shell - 3,
		-25,
		3
	]) difference() {
		cylinder(
			r=4.5,
			h=5,
			center=true
		);
		cylinder(
			r=relay_screw_d / 2,
			h=rpi_pos_z + 3,
			center=true
		);
	}
	translate ([
		-(box_x / 2) + box_shell + 3,
		2.5,
		3
	]) difference() {
		cylinder(
			r=4.5,
			h=5,
			center=true
		);
		cylinder(
			r=relay_screw_d / 2,
			h=rpi_pos_z + 3,
			center=true
		);
	}
	
	//	---- DS3231 Screw ----
	translate([
		-60,
		-14,
		4 / 2
	]) {
		translate([
			-14,
			-9,
			0
		]) difference() {
			cylinder(
			 r=3,
			 h=box_shell + 4,
			 center=true
			);
			cylinder(
			 r=1,
			 h=box_shell + 6,
			 center=true
			);
		}
		translate([
			-14,
			9,
			0
		]) difference() {
			cylinder(
			 r=3,
			 h=box_shell + 4,
			 center=true
			);
			cylinder(
			 r=1,
			 h=box_shell + 6,
			 center=true
			);
		}
		translate([
			12,
			9,
			0
		]) difference() {
			cylinder(
			 r=3,
			 h=box_shell + 4,
			 center=true
			);
			cylinder(
			 r=1,
			 h=box_shell + 6,
			 center=true
			);
		}
		translate([
			12,
			-9,
			0
		]) difference() {
			cylinder(
			 r=3,
			 h=box_shell + 4,
			 center=true
			);
			cylinder(
			 r=1,
			 h=box_shell + 6,
			 center=true
			);
		}
	}
}
// ----- /BOTTOM -----

// ----- FRONT -----
translate([
	0,
	(box_y / 2) - (box_shell / 2),
	(box_z / 2) - (box_shell / 2)
]) {
	difference() {
		cube([
			box_x,
			box_shell,
			box_z - box_shell
		], center=true);
		translate([
			(box_x / 2) - box_shell,
			0,
			-((box_z - box_shell) / 2) + box_shell + rpi_pos_z + rpi_z
		]) {
			// ---- lan ----
			translate([
				-rpi_lan_relpos,
				0,
				rpi_lan_z / 2
			]) cube([
				rpi_lan_x,
				box_shell + 1,
				rpi_lan_z
			], center=true);
			// ---- usb 1 ----
			translate([
				-rpi_usb1_relpos,
				0,
				rpi_usb_z / 2
			]) cube([
				rpi_usb_x,
				box_shell + 1,
				rpi_usb_z
			], center=true);
			// ---- usb 2 ----
			translate([
				-rpi_usb2_relpos,
				0,
				rpi_usb_z / 2
			]) cube([
				rpi_usb_x,
				box_shell + 1,
				rpi_usb_z
			], center=true);
		}
	}
}
// ----- /FRONT -----

// ----- RIGHT -----

translate([
	(box_x / 2) - (box_shell / 2),
	0,
	(box_z / 2) - (box_shell / 2)
]) {
	difference() {
		cube([
			box_shell,
			box_y,
			box_z - box_shell
		], center=true);
		translate([
			0,
			(box_y / 2) - box_shell,
			-((box_z - box_shell) / 2) + box_shell + rpi_pos_z + rpi_z
		]) {
			// ---- jack 3.5 ----
			translate([
				0,
				-rpi_y + rpi_jack_relpos,
				((3.5 * 2) / 2) - (((3.5 * 2) - 5.5) / 2)
			]) rotate([
				90,
				0,
				90
			]) cylinder(
				r1=3.5 * 2,
				r2=3.5,
				h=5,
				center=true
			);
			// ---- hdmi ----
			translate([
				5,
				-rpi_y + rpi_hdmi_relpos - 0.5,
				(7 / 2) + 0.5
			]) {
				translate ([
					-6.5,
					-7,
					0
				]) rotate([
					0,
					0,
					60	
				]) cube([10, 5, 6.5], center=true);	
				rotate([
					90,
					0,
					90
				]) HDMI(10);	
			}
			// ---- micro usb ----
			translate([
				5,
				-rpi_y + rpi_musb_relpos - 0.5,
				(4 / 2) - 0.5
			]) {
				translate ([
					-6.5,
					-3,
					0
				]) rotate([
					0,
					0,
					60	
				]) cube([10, 5, 4], center=true);
				rotate([
					90,
					0,
					90
				]) micro_usb(10);
			}
		}
	}
}

// ----- /RIGHT -----

// ----- LEFT -----
translate([
	(-box_x / 2) + (box_shell / 2),
	0,
	(box_z / 2) - (box_shell / 2)
]) {
	difference() {
		translate ([
			0,
			-6,
			0	
		]) cube([
			box_shell,
			box_y + 12,
			box_z - box_shell
		], center=true);
		
		translate([
		 0,
		 -(box_y / 2) + box_shell + 3,
		 -(box_z / 2) + box_shell + 3 + ((8.5 - 6) / 2)
		]) {
			rotate([
				0,
				90,
				0
			]) cylinder(
				r=3,
				h=box_shell + 1,
				center=true
			);
			translate([
				0,
				0,
				11
			]) rotate([
				0,
				90,
				0
			]) cylinder(
				r=3,
				h=box_shell + 1,
				center=true
			);
			translate([
				0,
				0,
				11 + 11
			]) rotate([
				0,
				90,
				0
			]) cylinder(
				r=3,
				h=box_shell + 1,
				center=true
			);
			translate([
				0,
				0,
				11 + 11 + 11
			]) rotate([
				0,
				90,
				0
			]) cylinder(
				r=3,
				h=box_shell + 1,
				center=true
			);
			translate([
				0,
				0,
				11 + 11 + 11 + 11
			]) rotate([
				0,
				90,
				0
			]) cylinder(
				r=3,
				h=box_shell + 1,
				center=true
			);
		}
	}
	
	translate([
		box_x - (outlet * 3) - box_shell,
		-(box_y / 2) - 6,
		0
	]) cube([
		box_shell,
		12,
		box_z - box_shell
	], center=true);
}
// ----- /LEFT -----

// ----- BACK -----
translate([
	0,
	(-box_y / 2) + (box_shell / 2),
	box_z / 2
]) {
	// ---- back_body ----
	difference() {
		translate ([
			0,
			0,
			-box_shell / 2
		]) cube([
			box_x,
			box_shell,
			box_z - box_shell
		], center=true);
		// ---- Outlet Holes ----
		translate([
			(box_x - (outlet * 3)) / 2,
			0,
			0
		]) {
			// ---- outlet hole 1 ----
			cube([
				outlet - (outlet_screw * 2),
				box_shell + 2,
				box_z + 2
			], center=true);
			// ---- outlet hole 2 ----
			translate([
				outlet,
				0,
				0
			]) cube([
				outlet - (outlet_screw * 2),
				box_shell + 1,
				box_z + 1
			], center=true);
			// ---- outlet hole 3 ----
			translate([
				-outlet,
				0,
				0
			]) cube([
				outlet - (outlet_screw * 2),
				box_shell + 1,
				box_z + 1
			], center=true);
		}
		//	---- Back Terminal ----
		translate([
			-box_x / 2 + box_shell + 4.5 + 1.65 + box_shell,
			0,
			0
		]) {
			// ---- back terminal 1 ----		 
			translate([
				0,
				0,
				-(box_z / 2) + box_shell + 2.25
			]) {
				translate([
					1.5,
					0,
					0
				]) rotate([
					90,
					0,
					0
				]) cylinder(
					r=4.25,
					h=box_shell+1,
					center=true
				);
				translate([
					-1.25,
					0,
					0
				]) cube([
					5,
					box_shell+1,
					8.5
				], center=true); 
			}
			// ---- back terminal 2 ----		 
			translate([
				0,
				0,
				-(box_z / 2) + box_shell + 2.25 + 11
			]) {
				translate([
					1.5,
					0,
					0
				]) rotate([
					90,
					0,
					0
				]) cylinder(
					r=4.25,
					h=box_shell+1,
					center=true
				);
				translate([
					-1.25,
					0,
					0
				]) cube([
					5,
					box_shell+1,
					8.5
				], center=true); 
			}
			// ---- back terminal 3 ----		 
			translate([
				0,
				0,
				-(box_z / 2) + box_shell + 2.25 + 11 + 11
			]) {
				translate([
					1.5,
					0,
					0
				]) rotate([
					90,
					0,
					0
				]) cylinder(
					r=4.25,
					h=box_shell+1,
					center=true
				);
				translate([
					-1.25,
					0,
					0
				]) cube([
					5,
					box_shell+1,
					8.5
				], center=true); 
			}
			// ---- back terminal 4 ----		 
			translate([
				0,
				0,
				-(box_z / 2) + box_shell + 2.25 + 11 + 11 + 11
			]) {
				translate([
					1.5,
					0,
					0
				]) rotate([
					90,
					0,
					0
				]) cylinder(
					r=4.25,
					h=box_shell + 1,
					center=true
				);
				translate([
					-1.25,
					0,
					0
				]) cube([
					5,
					box_shell + 1,
					8.5
				], center=true); 
			}
			// ---- back terminal 5 ----		 
			translate([
				0,
				0,
				-(box_z / 2) + box_shell + 2.25 + 11 + 11 + 11 + 11
			]) {
				translate([
					1.5,
					0,
					0
				]) rotate([
					90,
					0,
					0
				]) cylinder(
					r=4.25,
					h=box_shell+1,
					center=true
				);
				translate([
					-1.25,
					0,
					0
				]) cube([
					5,
					box_shell+1,
					8.5
				], center=true); 
			}
		}
		// ---- /back_body ----
	}
	//	---- Back Screws ----
	translate([
		(box_x - (outlet * 3)) / 2,
		0,
		0
	]) {
		// ---- back_outlet_screw_1 ----
		translate([
			(outlet / 2) - outlet_screw + (outlet_screw_holder / 2),
			-2,
			0
		]) difference() {
		cube([
			outlet_screw_holder,
			box_shell,
			12.5
			], center=true);
			rotate([
				90,
				0,
				0
			]) cylinder(
				h=5,
				r=1,
				center=true
			);
			translate([
				0,
				-3,
				-6
			]) rotate([
				0,
				90,
				0
			]) cylinder(
				h=8,
				r=3,
				center=true
			);
		}
		// ---- /back_outlet_screw_1 ----
		// ---- back_outlet_screw_2 ----
		translate([
			-((outlet / 2) - outlet_screw + (outlet_screw_holder / 2)),
			-2,
			0
		]) difference() {
		cube([
			outlet_screw_holder,
			box_shell,
			12.5
			], center=true);
			rotate([
				90,
				0,
				0
			]) cylinder(
				h=5,
				r1=1,
				r2=1,
				center=true
			);
			translate([
				0,
				-3,
				-6
			]) rotate([
				0,
				90,
				0
			]) cylinder(
				h=8,
				r1=3,
				r2=3,
				center=true
			);
		}
		// ---- /back_outlet_screw_2 ----
		// ---- back_outlet_screw_3 ----
		translate([
			(outlet / 2) + outlet_screw - (outlet_screw_holder / 2),
			-2,
			0
		]) difference() {
		cube([
			outlet_screw_holder,
			box_shell,
			12.5
			], center=true);
			rotate([
				90,
				0,
				0
			]) cylinder(
				h=5,
				r1=1,
				r2=1,
				center=true
			);
			translate([
				0,
				-3,
				-6
			]) rotate([
				0,
				90,
				0
			]) cylinder(
				h=8,
				r1=3,
				r2=3,
				center=true
			);
		}
		// ---- /back_outlet_screw_3 ----
		// ---- back_outlet_screw_4 ----
		translate([
			-((outlet / 2) + outlet_screw - (outlet_screw_holder / 2)),
			-2,
			0
		]) difference() {
		cube([
			outlet_screw_holder,
			box_shell,
			12.5
			], center=true);
			rotate([
				90,
				0,
				0
			]) cylinder(
				h=5,
				r1=1,
				r2=1,
				center=true
			);
			translate([
				0,
				-3,
				-6
			]) rotate([
				0,
				90,
				0
			]) cylinder(
				h=8,
				r1=3,
				r2=3,
				center=true
			);
		}
		// ---- /back_outlet_screw_4 ----
		// ---- back_outlet_screw_5 ----
		translate([
			outlet + (outlet / 2) - outlet_screw + (outlet_screw_holder / 2),
			-2,
			0
		]) difference() {
		cube([
			outlet_screw_holder,
			box_shell,
			12.5
			], center=true);
			rotate([
				90,
				0,
				0
			]) cylinder(
				h=5,
				r1=1,
				r2=1,
				center=true
			);
			translate([
				0,
				-3,
				-6
			]) rotate([
				0,
				90,
				0
			]) cylinder(
				h=8,
				r1=3,
				r2=3,
				center=true
			);
		}
		// ---- /back_outlet_screw_5 ----
		// ---- back_outlet_screw_6 ----
		translate([
			-(outlet + (outlet / 2) - outlet_screw + (outlet_screw_holder / 2)),
			-2,
			0
		]) difference() {
		cube([
			outlet_screw_holder,
			box_shell,
			12.5
			], center=true);
			rotate([
				90,
				0,
				0
			]) cylinder(
				h=5,
				r1=1,
				r2=1,
				center=true
			);
			translate([
				0,
				-3,
				-6
			]) rotate([
				0,
				90,
				0
			]) cylinder(
				h=8,
				r1=3,
				r2=3,
				center=true
			);
		}
		// ---- /back_outlet_screw_6 ----
	}
}
// ----- /BACK -----

// ----- TOP -----
translate([
	0,
	0,
	box_z - (box_shell / 2)
]) {
	difference() {
		cube([
			box_x,
			box_y,
			box_shell
		], center=true);
		translate([
			-43,
			-(box_y / 2) + box_shell,
			1.75
		]) linear_extrude(
			height = 0.75
		) text(
			"B1",
			font="Raleway",
			size=5
		);
		translate([
			16,
			-(box_y / 2) + box_shell,
			1.75
		]) linear_extrude(
			height = 0.75
		) text(
			"B2",
			font="Raleway",
			size=5
		);
		translate([
			73,
			-(box_y / 2) + box_shell,
			1.75
		]) linear_extrude(
			height = 0.75
		) text(
			"B3",
			font="Raleway",
			size=5
		);
	}
	translate([
		box_x / 2 - box_shell - 3,
		-25,
		-box_z / 2 + 2 + 7.5 - 3.75
	]) difference() {
		cylinder(
			r=3,
			h=box_z - (box_shell * 2) - 7,
			center=true
		);
		cylinder(
			r=1,
			h=box_z - (box_shell * 2),
			center=true
		);
	}
	translate([
		-(box_x / 2) + box_shell + 3,
		2.5,
		-box_z / 2 + 2 + 7.5 - 3.75
	]) difference() {
		cylinder(
			r=3,
			h=box_z - (box_shell * 2) - 7,
			center=true
		);
		cylinder(
			r=1,
			h=box_z - (box_shell * 2),
			center=true
		);
	}
	translate([
		(box_x / 2) - box_shell - 1.65,
		0,
		-3
	]) cube([
		3,
		box_shell * 2,
		3
	], center=true);
	translate([
		-(box_x / 2) + box_shell + 1.65,
		-20,
		-3
	]) cube([
		3,
		box_shell * 2,
		3
	], center=true);
	translate([
		-(box_x / 2) + box_shell + 1.65,
		20,
		-3
	]) cube([
		3,
		box_shell * 2,
		3
	], center=true);
	translate([
		-(box_x / 2) + box_shell + 1.65 + 20,
		-50,
		-3
	]) cube([
		3,
		box_shell * 2,
		3
	], center=true);
	translate([
		0,
		box_y / 2 - box_shell - 1.65,
		-3
	]) cube([
		box_shell * 2,
		3,
		3
	], center=true);
	translate([
		box_x / 3,
		box_y / 2 - box_shell - 1.65,
		-3
	]) cube([
		box_shell * 2,
		3,
		3
	], center=true);
	translate([
		-box_x / 3,
		box_y / 2 - box_shell - 1.65,
		-3
	]) cube([
		box_shell * 2,
		3,
		3
	], center=true);
	translate([
		-box_x / 3,
		-(box_y / 2) + box_shell + 1.65,
		-3
	]) cube([
		box_shell * 2,
		3,
		3
	], center=true);
	translate([
		-10,
		-(box_y / 2) + box_shell + 1.65,
		-3
	]) cube([
		box_shell * 2,
		3,
		3
	], center=true);
	translate([
		47.5,
		-(box_y / 2) + box_shell + 1.65,
		-3
	]) cube([
		box_shell * 2,
		3,
		3
	], center=true);
}
